﻿using System.Web.Optimization;

namespace Apex
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                "~/scripts/lib/jquery/dist/jquery.js",
                "~/scripts/jq-serialize.js",
                "~/scripts/semantic.js",
                "~/scripts/site.js"));

            // Layout page
            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                "~/scripts/lib/jquery/dist/jquery.js",
                "~/scripts/jq-serialize.js",
                "~/scripts/semantic.js",
                "~/scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/layout").Include(
                "~/content/semantic/semantic.css",
                "~/content/font-awesome.css",
                "~/content/site.css"));

            // Tools
            bundles.Add(new ScriptBundle("~/bundles/tools").Include(
                "~/scripts/tools.js",
                "~/scripts/calendar.min.js",
                "~/scripts/UserManagement/manageUserRequests.js",
                "~/scripts/UserManagement/manageOtherUsers.js",
                "~/scripts/UserManagement/ManageSubclasses.js",
                "~/scripts/UserManagement/RestrictUserToBranch.js",
                "~/scripts/UserManagement/manageMyAccount.js",
                "~/scripts/UserManagement/auditTrails.js",
                "~/scripts/Amu/xonomy.js",
                "~/scripts/amu/amu.js"));

            bundles.Add(new StyleBundle("~/Content/tools").Include(
                "~/content/tools.css",
                "~/content/calendar.css",
                "~/content/xonomy.css"));


            BundleTable.EnableOptimizations = false;
        }
    }
}
