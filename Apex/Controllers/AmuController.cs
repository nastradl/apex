﻿using Apex.Enums;
using Apex.Extensions;
using Apex.Helpers;
using Apex.Services;
using Apex.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Apex.Controllers
{
    [Authorized]
    public class AmuController : BaseController
    {
        private readonly AuthService _auth = new AuthService();
        private readonly BranchService _rep = new BranchService();

        #region Enrolment

        [ClaimRequirement("Amu", "BackToApproval")]
        public ActionResult BackToApproval()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToApproval",
                UpdateUrl = "UpdateBackToApproval",
                Title = "Back To Approval",
                AllowEditLocation = true,
                ShowLocation = true
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public ActionResult BackToAfisCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToAfisCheck",
                Title = "Back To Afis Check"
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public ActionResult BackToPaymentCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPaymentCheck",
                UpdateUrl = "UpdateBackToPayment",
                Title = "Back To Payment"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public ActionResult EditEnrolmentRecord()
        {
            var states = _auth.GetStates();
            var titles = _auth.GetTitles();
            var model = new DetailedRecord
            {
                SearchUrl = "SearchEnrolmentRecord",
                PageTitle = "Edit Enrolment Record",
                States = states,
                Titles = titles
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "LocalQuery")]
        public ActionResult LocalQuery()
        {
            var model = new Record
            {
                SearchUrl = "SearchLocalQuery",
                Title = "Local Query",
                Branches = Helper.GetBranches()
            };

           return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public ActionResult EditBioRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchEditBioRecord",
                Title = "Edit Bio Record"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ReverseEditBioRecord")]
        public ActionResult ReverseEditBioRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchReverseEditBioRecord",
                Title = "Reverse Edit Bio Record Change",
                ShowSecondFormNo = false
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public ActionResult ModifyEnrolmentFile()
        {
            var model = new Record
            {
                SearchUrl = "SearchModifyEnrolmentFile",
                Title = "Modify Enrolment File"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewXML.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "AnalyseRecord")]
        public ActionResult AnalyseRecord()
        {
            var states = _auth.GetStates();
            var titles = _auth.GetTitles();
            var model = new DetailedRecord
            {
                SearchUrl = "SearchAnalyseRecord",
                PageTitle = "Analyse Enrolment Records",
                ShowMultiMessage = true,
                States = states,
                Titles = titles
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseAnaylseView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public ActionResult ResetStagecode()
        {
            var model = new Record
            {
                SearchUrl = "SearchResetStagecode",
                Title = "Reset Stagecode"
            };

           return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ManuallyFail")]
        public ActionResult ManuallyFail()
        {
            var model = new Record
            {
                SearchUrl = "SearchManuallyFail",
                Title = "Manually Fail Record"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }
        
        #endregion

        #region Booklet

        [ClaimRequirement("Amu", "BackToPerso")]
        public ActionResult BackToPerso()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPerso",
                Title = "Back To Perso",
                ShowPassportNo = true,
                ShowSearchByDocNo = true,
                AllowEditLocation = true,
                ShowPersoStageCode = true,
                ShowDocStageCode = true,
                ShowStageCode = false,
                ShowPersoLocation = true,
                Branches = Helper.GetBranches()
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "QueryBookletInfo")]
        public ActionResult QueryBookletInfo()
        {
            var model = new Record
            {
                SearchUrl = "SearchQueryBookletInfo",
                Title = "Query Booklet Info",
                ShowStageCode = true,
                ShowBoxSn = true,
                ShowUpdateButton = false,
                Branches = Helper.GetBranches()
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public ActionResult OverideBookletSequence()
        {
            var model = new Record
            {
                SearchUrl = "SearchOverideBookletSequence",
                Title = "Overide Booklet Sequence",
                Branches = Helper.GetBranches(),
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public ActionResult BookletToPersoQueue()
        {
            var model = new Record
            {
                SearchUrl = "SearchBookletToPersoQueue",
                Title = "Return Booklet To Perso Queue",
                Branches = Helper.GetBranches()
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "InventoryManifest")]
        public ActionResult InventoryManifest()
        {
            var model = new Record
            {
                SearchUrl = "SearchInventoryManifest",
                Title = "Update Branch Inventory Manifest",
                Branches = Helper.GetBranches()
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateInventoryView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditBookletChipNo")]
        public ActionResult EditBookletChipNo()
        {
            var model = new Record
            {
                SearchUrl = "SearchEditBookletChipNo",
                Title = "Edit Booklet ChipNo",
                ShowBoxSn = true,
                Branches = Helper.GetBranches()
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }


        #endregion

        #region Personalisation

        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public ActionResult ResendJobForPrinting()
        {
            var model = new Record
            {
                SearchUrl = "SearchResendJobForPrinting",
                Title = "Resend Job For Printing",
                ShowStageCode = false,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "SendToIssuance")]
        public ActionResult SendToIssuance()
        {
            var model = new Record
            {
                SearchUrl = "SearchSendToIssuance",
                Title = "Search Send To Issuance",
                ShowStageCode = true,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        #endregion

        #region Issuance    

        [ClaimRequirement("Amu", "IssuePassport")]
        public ActionResult IssuePassport()
        {
            var model = new Record
            {
                SearchUrl = "SearchIssuePassport",
                Title = "Search Issue Passport",
                ShowStageCode = true,
                ShowSearchByDocNo = true,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true,
                Branches = Helper.GetBranches()
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public ActionResult FixWrongPassportIssued()
        {
            var model = new Record
            {
                SearchUrl = "SearchFixWrongPassportIssued",
                Title = "Fix Wrong Passport Issued",
                ShowStageCode = false
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        #endregion

        #region Intervention

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public ActionResult CreateReissueRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchCreateReissueRecord",
                Title = "Create reissue record",
                Branches = Helper.GetBranches()
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BindReissueRecord")]
        public ActionResult BindReissueRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchBindReissueRecord",
                Title = "Bind reissue record"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseBindReissueView.cshtml", model)
            });
        }

        #endregion

        #region Administration

        [ClaimRequirement("Amu", "ManageUser")]
        public ActionResult ManageUser()
        {
            var model = new Record
            {
                SearchUrl = "SearchManageUser",
                Title = "Manage User",
                Branches = Helper.GetBranches(),
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewUser.cshtml", model)
            });
        }

        #endregion

        #region Payment Management

        [ClaimRequirement("Amu", "PaymentQuery")]
        public ActionResult PaymentQuery()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQuery",
                Title = "Search Payment Query"
            };

            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                model.IsAdmin = true;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public ActionResult ResetPayment()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQueryReset",
                Title = "Search Payment Query"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditPaymentRecord")]
        public ActionResult EditPaymentRecord()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQueryEdit",
                Title = "Search Payment Query"
            };

            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                model.IsAdmin = true;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        #endregion

        // Json
        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public ActionResult SearchBackToAfisCheck(string formno)
        {
            if (string.IsNullOrEmpty(formno))
                return null;

            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchBackToAfisCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;
            

            // add update url
            record.UpdateUrl = "UpdateBackToAfisCheck";
            
            if (record.StageCode != "EM2001")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = $"Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To AFis Check";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }
        
        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public ActionResult UpdateBackToAfisCheck(Record record)
        {
            var result = _rep.UpdateAfisRecord(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "AnalyseRecord")]
        public async Task<ActionResult> SearchAnalyseRecord(string formno)
        {
            var formnos = formno.Split(',').Select(x => x.Trim()).ToList();

            if (formnos.Count > 4)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "Maximum of FOUR forms please"
                });

            var records = await _rep.SearchEnrolementForAnalysis(formnos, int.Parse(User.Identity.GetUserId()));

            if (records == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "Could not connect to branch"
                });

            if (records.Count == 0)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No records found matching entered search"
                });

            var json = Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseAnalyseViewResult.cshtml", records)
            });

            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        [ClaimRequirement("Amu", "BackToApproval")]
        public ActionResult SearchBackToApproval(string formno)
        {
            var record = _rep.SearchForApprovalRecord(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your search",
                });

            var err = CheckNoResultFound(record);
            if (err != null)
                return Json(new
                {
                    success = 1,
                    error = record.Error,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
                });

            // add update url
            record.UpdateUrl = "UpdateBackToApproval";
            record.AllowEditLocation = true;
            record.ShowLocation = true;
            string error = string.Empty;
            bool badstagecode = false;
            
            if (!string.IsNullOrEmpty(record.Error))
                error = record.Error;

            if (record.StageCode != "EM4000")
            {
                badstagecode = true;
                error = $"Cannot alter this record. Status requirements not met";
            }

            if (string.IsNullOrEmpty(error))
            {
                record.UpdateMsg = "Send Back To Approval";
                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
                });
            }

            return Json(new
                {
                    success = 1,
                    error,
                    badstagecode,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
                });
        }

        [ClaimRequirement("Amu", "BackToApproval")]
        public ActionResult UpdateBackToApproval(Record record)
        {
            var result = _rep.UpdateBackToApproval(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPerso")]
        public ActionResult SearchBackToPerso(string docno, string formno, string branchcode)
        {
            var record = new Record();

            if (!string.IsNullOrEmpty(formno))
            { 
                record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0, 3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchBackToPerso);
                branchcode = formno.Substring(0, 3);
            }

            if (!string.IsNullOrEmpty(docno))
                record = _rep.SearchEnrolementByDocNo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateBackToPerso";
            record.AllowEditLocation = true;
            record.ShowPersoLocation = true;
            record.ShowPersoStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowStageCode = false;
            record.ShowPassportNo = true;
           
            if (record.PersoProfileStageCode != "PM2000" || record.DocProfileStageCode != "PM2000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To Perso";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPerso")]
        public ActionResult UpdateBackToPerso(Record record)
        {
            var result = _rep.UpdateBackToPerso(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public ActionResult SearchResetStagecode(string formno)
        {
            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchEnrolProfile);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;
            
            // add update url
            record.UpdateUrl = "UpdateResetStagecode";
            
            if (record.StageCode != "EM0801")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Reset Stagecode";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public ActionResult UpdateResetStagecode(Record record)
        {
            var result = _rep.UpdateResetStagecode(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        //[ClaimRequirement("Amu", "ResetStagecode")]
        //public ActionResult UpdateResetStagecode(Record record)
        //{
        //    var result = _rep.UpdateResetStagecode(record, int.Parse(User.Identity.GetUserId()));

        //    return Json(new
        //    {
        //        success = result,
        //        clearform = true
        //    });
        //}

        [ClaimRequirement("Amu", "ManuallyFail")]
        public ActionResult SearchManuallyFail(string formno)
        {
            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchEnrolProfile);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateManuallyFail";
            
            if (record.StageCode != "EM4000" && record.StageCode != "EM2000" && !record.StageCode.StartsWith("G"))
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Fail Record";
            record.ShowRemark = true;

            if (record.StageCode == "EM4000")
            {
                if (!string.IsNullOrEmpty(record.PassportNo))
                    return Json(new
                    {
                        success = 1,
                        enable = true,
                        page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                        error = "Status is EM4000: Passport number is " + record.PassportNo
                    });

                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Status is EM4000 but no passport number found"
                });
            }
            
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        //[ClaimRequirement("Amu", "ManuallyFail")]
        //public ActionResult UpdateManuallyFail(Record record)
        //{
        //    if (string.IsNullOrEmpty(record.Remark))
        //    {
        //        return Json(new
        //        {
        //            success = 0,
        //            error = "Please enter a remark for this change"
        //        });
        //    }

        //    var result = _rep.UpdateManuallyFail(record, int.Parse(User.Identity.GetUserId()));

        //    return Json(new
        //    {
        //        success = result,
        //        clearform = true
        //    });
        //}


        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public ActionResult SearchFixWrongPassportIssued(string formno)
        {
            var record = _rep.SearchFixWrongPassportIssued(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateFixWrongPassportIssued";
            record.ShowStageCode = false;
            
            if (record.DocStageCode2 != "EM5000" || record.DocTwoStageCode != "EM5000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Fix Wrong Passport Issued";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public ActionResult UpdateFixWrongPassportIssued(Record record)
        {
            var result = _rep.UpdateFixWrongPassportIssued(record, int.Parse(User.Identity.GetUserId()));

            if (result == 3)
                return Json(new
                {
                    success = 0,
                    error = "Only one passport linked to this form number"
                });

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public ActionResult SearchBackToPaymentCheck(string formno)
        {
            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateBackToPaymentCheck";

            if (record.StageCode != "EM0801" && record.StageCode != "EM1000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To Payment Check";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public ActionResult UpdateBackToPaymentCheck(Record record)
        {
            var result = _rep.UpdateBackToPaymentCheck(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "InventoryManifest")]
        public ActionResult SearchInventoryManifest(string branchcode)
        {
            try
            {
                var inventoryserver = ConfigurationManager.AppSettings["inventoryserver"];

                if (ConfigurationManager.AppSettings["useLaptopAsBranch"] == "true")
                    branchcode = "369";

                // Get all XML files starting with this branch code
                string path = $"\\\\{inventoryserver}";

                // Get all files starting with this branch code
                var xmls = new DirectoryInfo(path).GetFiles($"[{branchcode}]*.xml");

                if (xmls.Length == 0)
                    return Json(new
                    {
                        success = 0,
                        error = "Coud not access any files for this branch"
                    });

                var list = xmls.OrderByDescending(x => x.CreationTime).Select(xml => xml.Name.Substring(6, 19))
                    .ToList();

                var record = new Record
                {
                    BranchCode = branchcode,
                    UpdateUrl = "UpdateInventoryManifest",
                    PassportRanges = list,
                    UpdateMsg = "Upload XML Data"
                };


                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateInventoryResult.cshtml", record)
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    error = ex.Message
                });
            }
        }

        [ClaimRequirement("Amu", "InventoryManifest")]
        public ActionResult UpdateInventoryManifest(Record record)
        {
            if (string.IsNullOrEmpty(record.SelectedXml))
                return Json(new
                {
                    success = 0,
                    error = "Please select an XML file"
                });

            var result = _rep.UpdateInventoryManifest(record, int.Parse(User.Identity.GetUserId()));

            if (result == 2)
            {
                return Json(new
                {
                    success = 0,
                    error = "This batch has already been uploaded"
                });
            }

            if (result == 3)
            {
                return Json(new
                {
                    success = 0,
                    error = "Upload was interupted and did not complete. Please run this XML file again"
                });
            }

            return Json(new
            {
                success = result,
                clearform = false
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public ActionResult SearchEnrolmentRecord(string formno)
        {
            //if (ConfigurationManager.AppSettings["GetDateChanged"] == "1")
            //{
            //    var dates = _rep.GetDateChanged();
            //    return Json(new
            //    {
            //        success = 1,
            //        page = Helper.JsonPartialView(this, "~/Views/Temp/_ShowDateChanges.cshtml", dates)
            //    });
            //}

            //if (ConfigurationManager.AppSettings["GetNullOtherName"] == "1")
            //{
            //    _rep.GetNullOthername(true);
            //    return null;
            //}

            //if (ConfigurationManager.AppSettings["SetFormNo"] == "1")
            //{
            //    _rep.SetFormNoAudit(1);
            //    return null;
            //}
            
            //if (true)
            //{
            //    _rep.UpdateOldFormNo();
            //    return null;
            //}

            int userid = int.Parse(User.Identity.GetUserId());
            string branch = formno.Substring(0, 3);
            var record = _rep.SearchEnrolementByFormNoDetailed(formno, branch, userid);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.States = _auth.GetStates();
            record.Titles = _auth.GetTitles();
            record.UpdateUrl = "UpdateEnrolmentRecord";
            
            if (record.StageCode == "EM5000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    enable = false,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateViewResult.cshtml", record),
                    error = $"Cannot alter this record. It has a status of { Helper.GetStageCodeDescription(record.StageCode)}"
                });

            // check if record has perso or docprofile
            var perso = _rep.GetPersoProfileWithFormNo(formno, branch, userid);
            if (perso != null && perso.Stagecode != "PM2001")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    enable = false,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateViewResult.cshtml", record),
                    error = $"Cannot alter this record. It has Doc/Perso but not { Helper.GetStageCodeDescription("PM2001")}"
                });

            // Save temp record to compare when updating
            Session["TempRecord"] = record;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public ActionResult UpdateEnrolmentRecord(DetailedRecord record)
        {
            // a few checks requested by Mina
            if (record.Sex == "F" && record.Title == "Mr")
                return Json(new
                {
                    success = 0,
                    error = "A female cannot be 'Mr'"
                });

            if (record.Sex == "M" && record.Title == "Mrs")
                return Json(new
                {
                    success = 0,
                    error = "A male cannot be 'Mrs'"
                });

            var result = _rep.UpdateEnrolmentRecord(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = false
            });
        }


        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public ActionResult SearchResendJobForPrinting(string formno)
        {
            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateResendJobForPrinting";
            record.ShowStageCode = false;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;
            
            if (record.PersoProfileStageCode != "PM1000" || record.DocProfileStageCode != "PM1000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change stage code: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }
                

            record.UpdateMsg = "Resend Job For Printing";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public ActionResult UpdateResendJobForPrinting(Record record)
        {
            var result = _rep.UpdateResendJobForPrinting(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "IssuePassport")]
        public ActionResult SearchIssuePassport(string docno, string formno, string branchcode)
        {
            var record = new Record();

            if (!string.IsNullOrEmpty(formno))
                record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            if (!string.IsNullOrEmpty(docno))
                record = _rep.SearchEnrolementByDocNo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateIssuePassport";
            record.ShowStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;

            if (record.DocProfiles != null)
            {
                var docprofilefound = false;
                var docprofilefound5000 = 0;

                foreach (var docprofile in record.DocProfiles)
                {
                    // Perso stagecode should be PM2000
                    var docperso = _rep.GetPersoProfile(docprofile.Docno.ToUpper(), record.BranchCode, int.Parse(User.Identity.GetUserId()));

                    if (docperso == null)
                        continue;

                    if (docperso.Stagecode == "PM2000")
                    {
                        record.PassportNo = docprofile.Docno.ToUpper();
                        record.PassportType = docprofile.Doctype;
                        record.DocProfileStageCode = docprofile.Stagecode;
                        record.PersoProfileStageCode = docperso.Stagecode;
                        docprofilefound = true;
                        break;
                    }

                    if (docperso.Stagecode == "EM5000")
                        docprofilefound5000++;
                 
                }

                if (!docprofilefound)
                {
                    string error = "Booklet not ready for issuance";

                    if (docprofilefound5000 == 1)
                    {
                        error = "Booklet already issued issued";
                    }

                    if (docprofilefound5000 > 1)
                    {
                        error = "Multiple booklets issued";
                    }

                    return Json(new
                    {
                        success = 1,
                        badstagecode = true,
                        page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                        error
                    });
                }
            }

            if (record.PersoProfileStageCode != "PM2000" || record.DocProfileStageCode != "PM2000" || record.StageCode != "EM4000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change stage code: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }


            record.UpdateMsg = "Issue Passport";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "IssuePassport")]
        public ActionResult UpdateIssuePassport(Record record)
        {
            var result = _rep.UpdateIssuePassport(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "SendToIssuance")]
        public ActionResult SearchSendToIssuance(string formno)
        {
            var record = _rep.SearchEnrolementByFormNo(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()), (int)EnumAuditEvents.SearchSendToIssuance);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateSendToIssuance";
            record.ShowStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;

            if (record.PersoProfileStageCode != "PM2001" && record.PersoProfileStageCode != "PM1000" 
                || record.DocProfileStageCode != "PM2001" && record.DocProfileStageCode != "PM1000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change status: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }


            record.UpdateMsg = "Send To Issuance";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "SendToIssuance")]
        public ActionResult UpdateSendToIssuance(Record record)
        {
            var result = _rep.UpdateSendToIssuance(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        //[ClaimRequirement("Amu", "ManageUser")]
        //public ActionResult GetBranchUsers(string branch)
        //{
        //    var result = _rep.GetBranchUsers(branch, int.Parse(User.Identity.GetUserId()));

        //    if (result == null)
        //    {
        //        return Json(new
        //        {
        //            success = "false"
        //        });
        //    }

        //    var branchusers = result.Select(user => new JsonResponse
        //        {
        //            Name = user.LoginName,
        //            Value = user.LoginName
        //        }).ToList();

        //    return Json(new
        //    {
        //        success = "true",
        //        results = branchusers
        //    });
        //}

        [ClaimRequirement("Amu", "ManageUser")]
        public ActionResult SearchManageUser(string username, string branchcode)
        {
            var record = _rep.SearchManageUser(username, branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateManageUser";
           
            record.UpdateMsg = "Update User";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewUserResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ManageUser")]
        public ActionResult GetBranchUsers(string id)
        {
            var users = _rep.GetBranchUsers(id, int.Parse(User.Identity.GetUserId()));
            var results = new List<JsonModel>();

            foreach (var user in users)
            {
                results.Add(new JsonModel
                {
                    text = user.LoginName.ToTitleCase(),
                    value = user.LoginName.ToTitleCase(),
                    name = user.LoginName.ToTitleCase() + " (" + user.FullName + ")"
                });
            }
            
            return Json(new
            {
                success = true,
                results
            }, JsonRequestBehavior.AllowGet);
        }

        [ClaimRequirement("Amu", "ManageUser")]
        public ActionResult UpdateManageUser(Record record)
        {
            var result = _rep.UpdateManageUser(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public ActionResult SearchModifyEnrolmentFile(string formno)
        {
            try
            {
                var record = _rep.SearchModfyEnrolment(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()));

                var err = CheckNoResultFound(record);
                if (err != null)
                    return err;

                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewXMLResult.cshtml", record)
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    error = ex.Message
                });
            }
        }

        [ValidateInput(false)]
        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public ActionResult UpdateModifyEnrolmentFile(Record record)
        {
            try
            {
                var result = _rep.UpdateModifyEnrolmentFile(record, int.Parse(User.Identity.GetUserId()));

                return Json(new
                {
                    success = result,
                    keepsuccess = true,
                    clearform = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    error = ex.Message
                });
            }
        }

        [ClaimRequirement("Amu", "ManuallyFail")]
        public ActionResult UpdateManuallyFail(Record record)
        {
            if (string.IsNullOrEmpty(record.Remark))
            {
                return Json(new
                {
                    success = 0,
                    error = "Please enter a remark for this change"
                });
            }

            var result = _rep.UpdateManuallyFail(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }
        
        [ClaimRequirement("Amu", "QueryBookletInfo")]
        public ActionResult SearchQueryBookletInfo(string docno, string branchcode)
        {
            var record = _rep.SearchQueryBookletInfo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            record.ShowUpdateButton = false;
            record.ShowBoxSn = true;
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }


        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public ActionResult SearchOverideBookletSequence(string docno, string branchcode)
        {
            var record = _rep.SearchQueryBookletInfo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            if (record.StageCode != "IM2000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (record.StageCode == "PM0500")
                    error = "Cannot alter this record as Booklet already personalised";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateOverideBookletSequence";
            record.UpdateMsg = "Overide Booklet Sequence";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public ActionResult UpdateOverideBookletSequence(Record record)
        {
            var result = _rep.UpdateOverideBookletSequence(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "EditBookletChipNo")]
        public ActionResult SearchEditBookletChipNo(string docno, string branchcode)
        {
            var record = _rep.SearchQueryBookletInfo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            record.ShowChipSn = true;
            record.UpdateUrl = "UpdateEditBookletChipNo";
            record.UpdateMsg = "Update Chip No";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "EditBookletChipNo")]
        public ActionResult UpdateEditBookletChipNo(Record record)
        {
            var result = _rep.UpdateEditBookletChipNo(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public ActionResult SearchCreateReissueRecord(string docno, string branchcode)
        {
            docno = docno.ToUpper();
            var record = _rep.SearchCreateReissueRecord(docno, branchcode, false, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            if (record.Passport.LocEnrolProfile.Stagecode != "EM5000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueUpdateViewResult.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateCreateReissueRecord";
            record.UpdateMsg = "Create Reissue Record";

            // get appreason list
            record.AppReasons = _rep.GetAppReasons();

            // get reasons for reissue
            record.ReissueReasons = _rep.GetReissueReasons();

            record.DocNo = docno;
            record.BranchCode = branchcode;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public ActionResult UpdateCreateReissueRecord(Record record)
        {
            string error = string.Empty;

            if (string.IsNullOrEmpty(record.EnrolBy))
                error = "Please select a reissue reasons (Enrolby)";


            if (record.AppReason == 0)
                error = "Please select an app reason";

            if (string.IsNullOrEmpty(record.DocPages))
                error = "Please select document pages";

            if (error != "")
                return Json(new
                {
                    success = 0,
                    error
                });

            var result = _rep.UpdateCreateReissueRecord(record, int.Parse(User.Identity.GetUserId()));

            if (result == null)
                return Json(new
                {
                    success = 0,
                    error = "Invalid data collected from central"
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueViewResult.cshtml", result),
                enable = true
            });
        }

        [ClaimRequirement("Amu", "BindReissueRecord")]
        public ActionResult SearchBindReissueRecord(string formno, string docno)
        {
            docno = docno.ToUpper();
            var branchCode = formno.Substring(0, 3);
            var record = _rep.SearchBindReissueRecord(docno.ToUpper(), formno, branchCode, false, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            if (record.Passport.LocEnrolProfile.Stagecode != "EM5000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateBindReissueRecord";
            record.UpdateMsg = "Bind Reissue Record";

            // get appreason list
            record.AppReasons = _rep.GetAppReasons();

            // get reasons for reissue
            record.ReissueReasons = _rep.GetReissueReasons();

            record.FormNo = formno;
            record.DocNo = docno;
            record.BranchCode = branchCode;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBindReissueViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public ActionResult UpdateBindReissueRecord(Record record)
        {
            string error = string.Empty;

            if (string.IsNullOrEmpty(record.EnrolBy))
                error = "Please select a reissue reasons (Enrolby)";


            if (record.AppReason == 0)
                error = "Please select an app reason";

            if (string.IsNullOrEmpty(record.DocPages))
                error = "Please select document pages";

            if (error != "")
            return Json(new
                {
                    success = 0,
                    error
                });

            var result = _rep.UpdateBindReissueRecord(record, int.Parse(User.Identity.GetUserId()));

            if (result == null)
                return Json(new
                {
                    success = 0,
                    error = "Could not gather required information from central"
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueViewResult.cshtml", result),
                enable = true
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public ActionResult SearchBookletToPersoQueue(string docno, string branchcode)
        {
            var record = _rep.SearchQueryBookletInfo(docno.ToUpper(), branchcode, int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            if (record.StageCode != "IM3000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record),
                    error
                });
            }

            record.UpdateMsg = "Send Booklet To PersoQueue";
            record.UpdateUrl = "UpdateBookletToPersoQueue";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public ActionResult UpdateBookletToPersoQueue(Record record)
        {
            var result = _rep.UpdateBookletToPersoQueue(record, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "LocalQuery")]
        public ActionResult SearchLocalQuery(string docno, string gender, string firstname, string surname, string formno, string branchcode)
        {
            // must have at least 2 variables
            var count = 0;

            if (!string.IsNullOrEmpty(docno)) count += 2;
            if (!string.IsNullOrEmpty(gender)) count++;
            if (!string.IsNullOrEmpty(firstname)) count++;
            if (!string.IsNullOrEmpty(surname)) count++;
            if (!string.IsNullOrEmpty(formno)) count += 2;

            if (count < 2)
                return Json(new
                {
                    success = 0,
                    error = "Please enter at least TWO fields to search"
                });

            // if its not formno, branchcode is required
            if (string.IsNullOrEmpty(formno) && string.IsNullOrEmpty(branchcode))
                return Json(new
                {
                    success = 0,
                    error = "Please select a branch"
                });

            branchcode = string.IsNullOrEmpty(formno) ? branchcode : formno.Substring(0, 3);

            var record = _rep.SearchLocalQuery(docno?.ToUpper(), branchcode, gender, firstname, surname, formno, int.Parse(User.Identity.GetUserId()));

            if (record == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "Could not connect to branch"
                });
            }

            if (record.Count == 0)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });


            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQueryResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "LocalQuery")]
        public ActionResult ViewLocalResult(string formno)
        {
            var record = _rep.SearchLocalQuery(null, formno.Substring(0, 3), null, null, null, formno, int.Parse(User.Identity.GetUserId()));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQueryResultTable.cshtml", record.First())
            });
        }

        [ClaimRequirement("Amu", "PaymentQuery")]
        public async Task<ActionResult> SearchPaymentQuery(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, int.Parse(User.Identity.GetUserId()));

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            SetOldValues(payment);

            payment.AllowEdit = false;
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }

        [ClaimRequirement("Amu", "EditPaymentRecord")]
        public async Task<ActionResult> SearchPaymentQueryEdit(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, int.Parse(User.Identity.GetUserId()));

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            SetOldValues(payment);

            // Get lists
            ViewBag.Offices = await _rep.GetPaymentOffices();
            ViewBag.States = await _rep.GetPaymentStates();

            payment.AllowEdit = true;
            payment.AllowUpdate = true;
            payment.UpdateMsg = "Save Changes";
            payment.UpdateUrl = "UpdatePaymentQuery";
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public async Task<ActionResult> SearchPaymentQueryReset(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, int.Parse(User.Identity.GetUserId()));

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            if (payment.Applicationstatus == "EM5000")
            {
                return Json(new
                {
                    success = 0,
                    error = "Cannot reset a record with status EM5000"
                });
            }

            payment.AllowUpdate = true;
            payment.UpdateMsg = "Reset Payment";
            payment.UpdateUrl = "UpdateResetPayment";

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }

        [ClaimRequirement("Amu", "PaymentQuery")]
        public async Task<ActionResult> UpdatePaymentQuery(Payment payment)
        {
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            var result = await _rep.UpdatePaymentQuery(payment, int.Parse(User.Identity.GetUserId()));

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Update failed"
                });
            }

            return Json(new
            {
                success = 1,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public async Task<ActionResult> UpdateResetPayment(Payment payment)
        {
            var result = await _rep.ResetPayment(payment.Aid, payment.RefId, int.Parse(User.Identity.GetUserId()));

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Reset failed"
                });
            }

            if (result == 2)
            {
                return Json(new
                {
                    success = 0,
                    error = "Cannot reset a record with status EM5000"
                });
            }

            return Json(new
            {
                success = 1,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public ActionResult SearchEditBioRecord(string formno, string secondformno)
        {
           if (string.IsNullOrEmpty(formno) || string.IsNullOrEmpty(secondformno))
                return Json(new
                {
                    success = 0,
                    hideform = true,
                    error = "Please fill both fields"
                });

            if (formno == secondformno)
                return Json(new
                {
                    success = 0,
                    hideform = true,
                    error = "Both form numbers should not be the same"
                });

            var record = _rep.SearchEditBioRecord(formno, secondformno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // apparently mina says this should be allowed. So...
            if (false) // (record.FaceReplace == null || record.SignatureReplace == null)
            {
                record.ShowUpdateButton = false;
                return Json(new
                {
                    success = 1,
                    error = "Replacement record must have Face and Signature",
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
                });
            }

            record.UpdateMsg = "Replace LEFT side with RIGHT";
            record.UpdateUrl = "UpdateEditBioRecord";
            record.BranchCode = formno.Substring(0,3);
            record.FormNo = formno;
            record.SecondFormNo = secondformno;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ReverseEditBioRecord")]
        public ActionResult SearchReverseEditBioRecord(string formno)
        {
            if (string.IsNullOrEmpty(formno))
                return Json(new
                {
                    success = 0,
                    error = "Please enter a form number"
                });

            var record = _rep.SearchReverseEditBioRecord(formno, formno.Substring(0,3), int.Parse(User.Identity.GetUserId()));

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            record.UpdateMsg = "Reverse edit bio transaction";
            record.UpdateUrl = "UpdateReverseEditBioRecord";
            record.BranchCode = formno.Substring(0,3);
            record.FormNo = formno;
            record.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public ActionResult UpdateEditBioRecord(Record record)
        {
            if (record.Revert == 1)
                return UpdateReverseEditBioRecord(record);

            var result = _rep.UpdateEditBioRecord(record, int.Parse(User.Identity.GetUserId()));

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Update failed"
                });
            }

            // get refresh
            var newrecord = _rep.SearchEditBioRecord(record.FormNo, record.SecondFormNo, record.BranchCode,
                int.Parse(User.Identity.GetUserId()));

            newrecord.UpdateMsg = "Replace LEFT side with RIGHT";
            newrecord.UpdateUrl = "UpdateEditBioRecord";
            newrecord.BranchCode = record.BranchCode;
            newrecord.FormNo = record.FormNo;
            newrecord.SecondFormNo = record.SecondFormNo;
            newrecord.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", newrecord)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public ActionResult UpdateReverseEditBioRecord(Record record)
        {
            var result = _rep.UpdateReverseEditBioRecord(record, int.Parse(User.Identity.GetUserId()));

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Revert failed"
                });
            }

            // get refresh
            var newrecord = _rep.SearchEditBioRecord(record.FormNo, record.SecondFormNo, record.BranchCode,
                int.Parse(User.Identity.GetUserId()));

            newrecord.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                hideonupdate = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", newrecord)
            });
        }

        private ActionResult CheckNoResultFound(Record record)
        {
            if (record == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No record found matching your request"
                });

            if (!string.IsNullOrEmpty(record.Error))
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = record.Error
                });

            return null;
        }

        private ActionResult CheckNoResultFound(DetailedRecord record)
        {
            if (record == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No record found matching your request"
                });

            if (!string.IsNullOrEmpty(record.Error))
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = record.Error
                });

            return null;
        }

        private void SetOldValues(Payment payment)
        {
            payment.OldSurname = payment.Lastname;
            payment.OldDob = payment.Dob;
            payment.OldExpiryDate = payment.Expirydate;
            payment.OldSex = payment.Sex;
            payment.OldFirstname = payment.Firstname;
            payment.OldProcessingcountry = payment.Processingcountry;
            payment.OldProcessingoffice = payment.Processingoffice;
            payment.OldProcessingstate = payment.Processingstate;
        }
    }
}
