﻿using Apex.Helpers;
using Apex.Models.AccountViewModels;
using Apex.Services;
using Apex.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Apex.Controllers
{
    [Authorized]
    public class HomeController : BaseController
    {
        private readonly BranchService _branch = new BranchService();
        private readonly AuthService _rep = new AuthService();
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [AllowAnonymous]
        public ActionResult Index()
        {
            var model = new RegistrationModel();

            if (Request.Url.Host.StartsWith("localhost"))
            {
                model.LoginViewModel = new LoginViewModel
                {
                    Email = "dlawal@irissmart.com",
                    Password = "damilola"
                };
            }

            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Tools");

            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Error()
        {
            if (User.Identity.IsAuthenticated && !Request.IsAjaxRequest())
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                new AuthService().UserLoggedOut(int.Parse(User.Identity.GetUserId()));
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult ErrorHandled(int code)
        {
            return new HttpStatusCodeResult(code);
        }

        public async Task<ActionResult> Tools(string section, string option)
        {
            var signedUser = await UserManager.FindByIdAsync(int.Parse(User.Identity.GetUserId()));

            if (signedUser == null)
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction(nameof(Index), "Home");
            }

            // Get user claims
            var claims = await UserManager.GetClaimsAsync(signedUser.Id);

            var subclasses = _rep.GetSubClasses();

            foreach (var subclass in subclasses)
            {
                subclass.Claims.RemoveAll(c => !claims.Select(x => x.Value).Contains(c.Action));
            }

            var model = new MenuModel
            {
                Username = signedUser.UserName,
                SubClasses = subclasses,
                Claims = claims.OrderBy(x => x.Value).ToList()
            };

            return View(model);
        }
    }
}
