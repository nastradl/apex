﻿using Apex.Enums;
using Apex.Extensions;
using Apex.Helpers;
using Apex.Models.Central;
using Apex.Services;
using Apex.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace Apex.Controllers
{
    [Authorized]
    public class UserManagementController : BaseController
    {
        private readonly AuthService _rep = new AuthService();

        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public ActionResult ManageMyAccount()
        {
            var user = UserManager.FindById(int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageMyAccount.cshtml", user)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public ActionResult ManageOtherUsers()
        {
            var users = _rep.GetUsers().Where(x => x.Id.ToString() != User.Identity.GetUserId()).ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageOtherUsersWrap.cshtml", users)
            });
        }

        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public ActionResult ManageUserRequests()
        {
            var requests = _rep.GetUserRequests();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageUserRequests.cshtml", requests)
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public ActionResult ManageSubclasses()
        {
            var subclass = _rep.GetSubClasses();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclassesWrap.cshtml", subclass.OrderBy(x => x.SubclassName).ToList())
            });
        }

        [ClaimRequirement("UserManagement", "RestrictUserToBranch")]
        public ActionResult RestrictUserToBranch() 
        {
            var users = _rep.GetUsers().Where(x => x.Id.ToString() != User.Identity.GetUserId()).ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_MemberBranchWrap.cshtml", users)
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult ViewErrorLogs()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["errorPerPage"]);
            var errors = _rep.GetErrorLogs(perpage, 0, "TimeStamp", 1);

            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, 1, perpage, errors.Total);
            errors.OrderBy = "TimeStamp";
            errors.CurrentPage = 1;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogs.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult ViewAuditTrails()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);
            var audits = _rep.GetAuditTrails(perpage, 0, "TimeStamp", 1, "", "");

            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;
            audits.SearchStartDate = DateTime.Today.ToString("dd/MMM/yyyy");
            audits.SearchEndDate = DateTime.Today.ToString("dd/MMM/yyyy");

            // Search values
            audits.UserList = _rep.GetUsers();
            audits.ActionsList = Enum.GetValues(typeof(EnumAuditEvents))
                .Cast<EnumAuditEvents>()
                .Select(v => v.ToString())
                .ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrails.cshtml", audits)
            });
        }


        // Json
        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public async Task<ActionResult> ApproveUserRequest(string selected, string reject)
        {
            bool rejection = reject == "1";

            if (string.IsNullOrEmpty(selected))
                return Json(new
                {
                    success = 0,
                    error = "No users found matching request"
                });

            // convert selected to list of ids
            var ids = selected.Split(',').Select(int.Parse).ToList();
            var emailcredentials = ConfigurationManager.AppSettings["email"];
            var emailpassword = Helper.DecryptConfig(ConfigurationManager.AppSettings["password"], false);

            using (var client = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailcredentials, emailpassword)
            })
            {
                if (rejection)
                {
                    foreach (var req in ids)
                    {
                        var request = _rep.GetUserRequest(req);

                        _rep.RejectUserRequest(int.Parse(User.Identity.GetUserId()), ids);

                        // send an email to user with registration information
                        var iris = ConfigurationManager.AppSettings["companyEmail"];
                        var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                        string subject = "Apex registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Unfortunately, your request to register with the Iris Apex tool has been denied. 
                                    Please contact an admin staff if you need to discuss this decision.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
                else
                {
                    // send an email to user with registration information
                    foreach (var req in ids)
                    {
                        var request = _rep.GetUserRequest(req);
                        string tempPassword = Helper.GenerateRandomString(6);
                        var result = await _rep.ApproveUserRequest(int.Parse(User.Identity.GetUserId()), ids, tempPassword);

                        if (result == -1)
                            return Json(new
                            {
                                success = 0,
                                error = "This user already exists in the database"
                            });
                        
                        var iris = ConfigurationManager.AppSettings["companyEmail"];
                        var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                        string subject = "Apex registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Your request to register with the Iris Apex tool has been approved. 
                                    Please log in with your email and this temporary password:</div>
                                    <div style='font-size:18px; font-weight: bold; margin: 10px 0px'>{tempPassword}</div>
                                    <div>Please change this to your own password in the ""Manage My Account"" section after you log in.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
            }
                
            // get updated list of requests
            var requests = _rep.GetUserRequests();
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageUserRequests.cshtml", requests)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public ActionResult GetUserPermissions(int id)
        {
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaims.cshtml", GetUserClaims(id))
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public ActionResult AddClaims(int id, string claims)
        {
            _rep.AddClaimsToUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public ActionResult RemoveClaims(int id, string claims)
        {
            _rep.RemoveClaimsFromUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public ActionResult SetUserLockout(int id, int lockout)
        {
            _rep.SetUserLockout(id, int.Parse(User.Identity.GetUserId()), lockout == 1);

            var users = _rep.GetUsers().Where(x => x.Id.ToString() != User.Identity.GetUserId()).ToList();
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageOtherUsers.cshtml", users)
            });
        }


        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public ActionResult UpdatePersonalDetails(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id.ToString() != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            var result = _rep.UpdateUserDetails(user);

            if (result == -1)
                return Json(new
                {
                    success = result,
                    error = "That username is already in user. Please enter a different one"
                });

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<ActionResult> ChangePassword(ApplicationUser user)
        {
            if (string.IsNullOrEmpty(user.CurrentPassword) || string.IsNullOrEmpty(user.NewPassword))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both current and new password"
                });

            // Confirm the correct user is making this change
            if (user.Id.ToString() != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            _rep.UpdateUserPassword(user);

            var result = await UserManager.ChangePasswordAsync(user.Id, user.CurrentPassword, user.NewPassword);

            if (result.Succeeded)
                return Json(new
                {
                    success = 1
                });

            return Json(new
            {
                success = 0,
                error = result.Errors.First()
            });
        }


        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult SearchAudit(int perpage, string searchuser, string searchaction, string searchstartdate, string searchenddate, string isprint, string printpage, string formno, string docno)
        {
            var param = new SearchParam
            {
                PerPage = perpage,
                UserId = searchuser,
                Actions = searchaction,
                StartDate = searchstartdate,
                EndDate = searchenddate,
                FormNo = formno,
                DocNo = docno,
                Page = 0,
                OrderBy = "TimeStamp"
            };

            var audits = _rep.SearchAuditTrails(param);
            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;

            Session["searchParams"] = param;

            return Json(new
            {
                success = 1,
                total = audits.Total.ToString("N0"),
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult GetInnerException(int id)
        {
            var error = _rep.GetErrorLog(id);

            return Json(new
            {
                success = 1,
                msg = error.InnerException
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public ActionResult GetSubclass(int id, string refreshmenu)
        {
            var sub = _rep.GetSubClass(id);
            var claims = _rep.GetClaimsList();

            var model = new EditSubclass
            {
                Id = sub.Id,
                SubclassName = sub.SubclassName,
                SelectedClaims = sub.Claims.OrderBy(x => x.Action).ToList(),
                UnSelectedClaims = claims.Where(x => x.Controller == "AMU" && sub.Claims.All(c => c.Id != x.Id)).OrderBy(x => x.Action).ToList()
            };

            if (refreshmenu == "1")
            {
                // Get user claims
                var signedUser = UserManager.FindById(int.Parse(User.Identity.GetUserId()));
                var userclaims = UserManager.GetClaims(signedUser.Id);

                // Remove any claims which end in "Admin" cos these are how I differentiate admin claims. There's obviously a better way to do this..but like... yeah. *sips coffee*.
                userclaims.Remove(userclaims.FirstOrDefault(x => x.Value.ToLower().EndsWith("admin")));

                var subclasses = _rep.GetSubClasses();

                foreach (var subclass in subclasses)
                {
                    subclass.Claims.RemoveAll(c => !userclaims.Select(x => x.Value).Contains(c.Action));
                }

                var menuModel = new MenuModel
                {
                    Username = signedUser.UserName,
                    SubClasses = subclasses.OrderBy(x => x.SubclassName).ToList(),
                    Claims = userclaims.OrderBy(x => x.Value).ToList(),
                };

                return Json(new
                {
                    success = 1,
                    page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclasses.cshtml", model),
                    menu = Helper.JsonPartialView(this, "~/Views/Shared/_SideMenu.cshtml", menuModel)
                });
            }
           
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclasses.cshtml", model)
            });
        }

        //[ClaimRequirement("UserManagement", "ManageSubclasses")]
        //public ActionResult AddClaimsToSubclass(int id, string claims)
        //{
        //    _rep.AddClaimsToSubclass(id, claims, User.Identity.GetUserId());

        //    return GetSubclass(id, "1");
        //}

        //[ClaimRequirement("UserManagement", "ManageSubclasses")]
        //public ActionResult RemoveClaimsFromSubclass(int id, string claims)
        //{
        //    _rep.RemoveClaimsFromSubclass(id, claims, User.Identity.GetUserId());

        //    return GetSubclass(id, "1");
        //}

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult GetErrorLogsPage(int page, string orderby, int reverse, int perpage = 50)
        {
            var errors = _rep.GetErrorLogs(perpage, page - 1, orderby, reverse);
            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, page, perpage, errors.Total);
            errors.OrderBy = orderby;
            errors.CurrentPage = page;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogsResult.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult GetAuditTrailPage(int page, string orderby, int reverse, string printpage, string isprint, int perpage = 50)
        {
            ViewAudits audits;

            if (string.IsNullOrEmpty(orderby))
                orderby = "Name";

            if (Session["searchParams"] is SearchParam searchparam)
            {
                searchparam.Page = page;
                searchparam.IsPrint = string.IsNullOrEmpty(isprint) ? 0 : int.Parse(isprint);
                searchparam.PrintPage = string.IsNullOrEmpty(printpage) ? 0 : int.Parse(printpage);
                audits = _rep.SearchAuditTrails(searchparam);
                audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, searchparam.IsPrint == 1 && searchparam.PrintPage == 0 ? audits.Total : searchparam.PerPage, audits.Total);
                audits.OrderBy = searchparam.OrderBy;
                audits.CurrentPage = page;

                if (!string.IsNullOrEmpty(isprint))
                {
                    audits.IsPrint = true;
                    string title = "Audit Logs";

                    if (audits.Audits.Count != 0)
                    {
                        if (!string.IsNullOrEmpty(searchparam.UserId))
                            title += $" with User: {audits.Audits[0].User.FirstName.ToTitleCase()} {audits.Audits[0].User.Surname.ToTitleCase()}";

                        if (!string.IsNullOrEmpty(searchparam.Actions))
                            title += " (Selected Actions)";
                        
                        if (!string.IsNullOrEmpty(searchparam.DocNo))
                            title += $" for {audits.Audits[0].DocNo}";

                        if (!string.IsNullOrEmpty(searchparam.FormNo))
                            title += $" for {audits.Audits[0].FormNo}";

                        if (!string.IsNullOrEmpty(searchparam.StartDate))
                            title += " from " + DateTime.Parse(searchparam.StartDate).ToString("dd/MM/yyyy");

                        if (!string.IsNullOrEmpty(searchparam.EndDate))
                        {
                            title += (string.IsNullOrEmpty(searchparam.StartDate) ? " up till " : " to ") +
                                     DateTime.Parse(searchparam.EndDate).ToString("dd/MM/yyyy");
                        }
                    }

                    audits.Title = title + $" ({audits.Audits.Count:N0})";
                }
            }
            else
            {
                audits = _rep.GetAuditTrails(perpage, page, orderby, reverse, printpage, isprint);
                audits.Page = new StaticPagedList<Audit>(audits.Audits, page + 1, perpage, audits.Total);
                audits.OrderBy = orderby;
                audits.CurrentPage = page;
                audits.Title = "Audit Logs for today";
            }
            
            return Json(new
            {
                success = 1,
                total = audits.Total,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public ActionResult AddClaimsToSubclass(int id, string claims)
        {
            if (string.IsNullOrEmpty(claims))
                return GetSubclass(id, "1");

            _rep.AddClaimsToSubclass(id, claims, int.Parse(User.Identity.GetUserId()));

            return GetSubclass(id, "1");
        }

        [ClaimRequirement("UserManagement", "RestrictUserToBranch")]
        public ActionResult GetMemberBranches(int id)
        {
            var memberBranch = _rep.GetMemberBranch(id);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_MemberBranch.cshtml", memberBranch)
            });
        }

        [ClaimRequirement("UserManagement", "RestrictUserToBranch")]
        public ActionResult RemoveBranchFromUser(int id, string branches)
        {
            _rep.RemoveBranchFromUser(id, branches, int.Parse(User.Identity.GetUserId()));

            return GetMemberBranches(id);
        }

        [ClaimRequirement("UserManagement", "RestrictUserToBranch")]
        public ActionResult AddBranchToUser(int id, string branches)
        {
            _rep.AddBranchToUser(id, branches, int.Parse(User.Identity.GetUserId()));

            return GetMemberBranches(id);
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public ActionResult RemoveClaimsFromSubclass(int id, string claims)
        {
            if (string.IsNullOrEmpty(claims))
                return GetSubclass(id, "1");

            _rep.RemoveClaimsFromSubclass(id, claims, int.Parse(User.Identity.GetUserId()));

            return GetSubclass(id, "1");
        }

        // Operations
        private EditUserClaimsWrap GetUserClaims(int id)
            {
                var claims = new List<EditUserClaims>();
                var user = UserManager.FindById(id);
                var userclaims = UserManager.GetClaims(user.Id);
                var allclaims = _rep.GetClaimsList();

                // selected
                foreach (var claim in userclaims)
                {
                    var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Type);

                    if (userclaim == null)
                    {
                        userclaim = new EditUserClaims
                        {
                            Controller = claim.Type
                        };

                        claims.Add(userclaim);
                    }

                    userclaim.SelectedClaims.Add(claim.Value);
                }

                // unselected
                foreach (var claim in allclaims)
                {
                    var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Controller);
                    if (userclaim == null)
                    {
                        userclaim = new EditUserClaims
                        {
                            Controller = claim.Controller
                        };

                        claims.Add(userclaim);
                    }

                    if (userclaim.Controller == claim.Controller && !userclaim.SelectedClaims.Contains(claim.Action))
                        userclaim.UnSelectedClaims.Add(claim.Action);
                }

                // order them
                foreach (var c in claims)
                {
                    c.SelectedClaims = c.SelectedClaims.OrderBy(x => x).ToList();
                    c.UnSelectedClaims = c.UnSelectedClaims.OrderBy(x => x).ToList();
                }

                return new EditUserClaimsWrap
                {
                    UserId = id.ToString(),
                    Claims = claims
                };
            }
    }
}