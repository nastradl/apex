﻿using System.ComponentModel;

namespace Apex.Enums
{
    public enum EnumAuditEvents
    {
        [Description("User logged in successfully")]
        LoginSuccess = 1,
        [Description("User logged out")]
        LogOut = 2,
        [Description("User failed to login with username and password")]
        LoginFailed = 3,
        [Description("User requested to be registered")]
        RequestAdded = 4,
        [Description("User request approved and new user created")]
        RequestApproved = 5,
        [Description("User request denied")]
        RequestDenied = 6,
        [Description("Assign privilege to user")]
        PrivilegeAssigned = 7,
        [Description("Revoke privilege from user")]
        PrivilegeRevoked = 8,
        [Description("Disable a user")]
        DisableUser = 9,
        [Description("User updated personal details")]
        PersonalDetailsUpdated = 10,
        [Description("User changed password")]
        PasswordChanged = 11,
        [Description("Search enrolprofile table")]
        SearchEnrolProfile = 12,
        [Description("Update enrolprofile stagecode from EM4000 to EM2000 and set enrollocationid to 9115001")]
        UpdateBackToApproval = 13,
        [Description("Update enrolprofile stagecode from EM0801 to EM0800")]
        UpdateBackToPaymentCheck = 14,
        [Description("Update persoprofile and docprofile stagecode from PM2000 to PM2001")]
        UpdateBackToPerso = 15,
        [Description("Update enrolprofile stagecode from EM2001 to EM1500")]
        UpdateAfisRecord = 16,
        [Description("Update record details")]
        UpdateRecordDetails = 17,
        [Description("Update persoprofile and docprofile stagecode from PM1000 to PM0500")]
        UpdateResendJobForPrinting = 18,
        [Description("Update docinventory stagecode from IM2000 to IM3000")]
        UpdateOverideBookletSequence = 19,
        [Description("Update docinventory stagecode from IM3000 to IM2000")]
        UpdateBookletToPersoQueue = 20,
        [Description("Update persoprofile and docprofile stagecode from PM2001/PM1000 to PM2000")]
        UpdateSendToIssuance = 21,
        [Description("Search local database")]
        LocalQuery = 22,
        [Description("Fix wrong passport issued")]
        UpdateFixWrongPassportIssued = 23,
        [Description("Search bioprofile table")]
        SearchEditBioRecord = 24,
        [Description("Update bioprofile Signature/Face")]
        UpdateEditBioRecord = 25,
        [Description("Revert back to previous bioprofile Signature/Face")]
        UpdateEditBioRecordRevert = 26,
        [Description("Search for a reversable bioprofile transaction")]
        SearchReverseEditBioRecord = 27,
        [Description("Search for user at branch")]
        SearchBranchUser = 28,
        [Description("Update branch user")]
        UpdateBranchUser = 29,
        [Description("Retreive appliction temp XML file")]
        SearchXmlRecord = 30,
        [Description("Edit XML file for application")]
        UpdateXmlFile = 31,
        [Description("Search for record for Reissue from central")]
        SearchCreateReissueRecord = 32,
        [Description("Create reissue record")]
        UpdateCreateReissueRecord = 33,
        [Description("Search for record for Bind Reissue from central")]
        SearchBindReissueRecord = 34,
        [Description("Create bind reissue record")]
        UpdateBindReissueRecord = 35,
        [Description("Search for record")]
        SearchBackToAfisCheck = 36,
        [Description("Search for record")]
        SearchBackToPerso = 37,
        [Description("Search for record")]
        SearchFixWrongPassportIssued = 38,
        [Description("Search for record")]
        SearchBackToPaymentCheck = 39,
        [Description("Search for record")]
        SearchSendToIssuance = 40,
        [Description("Search for payment")]
        SearchPaymentQuery = 41,
        [Description("Edit payment")]
        UpdatePayment = 42,
        [Description("Reset payment")]
        ResetPayment = 43,
        [Description("Reset stagecode")]
        UpdateResetStagecode = 44,
        [Description("Update Manually Fail")]
        UpdateManuallyFail = 45,
        [Description("Enable a user")]
        EnableUser = 46,
        [Description("Assign privilege to Subclass")]
        PrivilegeAssignedSubclass = 47,
        [Description("Revoke privilege from Subclass")]
        PrivilegeRevokedSubclass = 48,
        [Description("Upload XML to Branch Inventory")]
        UploadXmlToBrachInventory = 49,
        [Description("Delete Incomplete Perso")]
        DeleteIncompletePerso = 50,
        [Description("Assign branch to user")]
        BranchAssignedToUser = 51,
        [Description("Remove branch from user")]
        BranchRemovedFromUser = 52,
        [Description("Update chip serial number in DocInventory")]
        UpdateEditBookletChipNo = 53

    }
}
