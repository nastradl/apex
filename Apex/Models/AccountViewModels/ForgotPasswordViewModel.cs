﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
