﻿using Apex.Enums;
using Apex.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class AppReason
    {
        protected AppReason() { } 

        public AppReason(EnumAppReasons @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator AppReason(EnumAppReasons @enum) => new AppReason(@enum);

        public static implicit operator EnumAppReasons(AppReason listAppReason) => (EnumAppReasons)listAppReason.Id;
    }
}
