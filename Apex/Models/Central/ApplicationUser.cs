﻿using Apex.Extensions;
using AspNet.Identity.Dapper;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Apex.Models.Central
{
    public class ApplicationUser : IdentityMember
    {
        [DefaultValue(1)]
        public bool IsFirstTimeLogin { get; set; }

        [NotMapped]
        public string NewPassword { get; set; }

        [NotMapped]
        public string CurrentPassword { get; set; }

        [NotMapped]
        public string NewPasswordConfirm { get; set; }

        public string GetFullName()
        {
            return FirstName.ToTitleCase() + " " + Surname.ToTitleCase();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : DbManager
    {
        public ApplicationDbContext(string connectionName)
            : base(connectionName)
        {
        }

        public static ApplicationDbContext Create()
        {
            var conn = ConfigurationManager.AppSettings["useLaptopAsCentral"].ToLower() == "true"
                ? "AuthLocal"
                : "Auth";
            return new ApplicationDbContext(ConfigurationManager.ConnectionStrings[conn].ConnectionString);
        }
    }
}
