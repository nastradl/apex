﻿using Apex.Enums;
using Apex.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class AuditEvent
    {
        public AuditEvent() { } 

        public AuditEvent(EnumAuditEvents @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator AuditEvent(EnumAuditEvents @enum) => new AuditEvent(@enum);

        public static implicit operator EnumAuditEvents(AuditEvent listAuditEvents) => (EnumAuditEvents)listAuditEvents.Id;
    }
}
