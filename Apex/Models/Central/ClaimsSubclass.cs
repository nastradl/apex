﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class ClaimsSubclass
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string SubclassName { get; set; }

        public List<UserClaim> Claims { get; set; }
    }
}
