﻿using System;

namespace Apex.Models.Central
{
    public class EditBioRecordStore
    {
        public int Id { get; set; }
       
        public string FormNo { get; set; }

        public string UpdateWithFormNo { get; set; }

        public byte[] Signature { get; set; }

        public byte[] Face { get; set; }

        public byte[] FaceJ2k { get; set; }

        public DateTime DateChanged { get; set; }

        public int UserId { get; set; }
    }
}
