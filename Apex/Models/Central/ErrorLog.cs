﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class ErrorLog
    {
        public int Id { get; set; }

        public string Message { get; set; }

        [StringLength(100)]
        public string Type { get; set; }

        [StringLength(100)]
        public string Url { get; set; }

        public int UserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string InnerException { get; set; }

        [Required]
        [DefaultValue("getdate()")]
        public DateTime DateCreated { get; set; }
    }
}
