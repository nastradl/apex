﻿using System;
using System.Collections.Generic;

namespace Apex.Models.Central
{
    public class Privilege
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Controller { get; set; }

        public List<PrivilegeAction> Actions { get; set; }

        public bool AllowAllActions { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
