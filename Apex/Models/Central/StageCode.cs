﻿using Apex.Enums;
using Apex.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class StageCode
    {
        protected StageCode() { }

        public StageCode(EnumStageCodes @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator StageCode(EnumStageCodes @enum) => new StageCode(@enum);

        public static implicit operator EnumStageCodes(StageCode listStageCode) => (EnumStageCodes)listStageCode.Id;
    }
}
