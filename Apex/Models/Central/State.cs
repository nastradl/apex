﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class State
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
