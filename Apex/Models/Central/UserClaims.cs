﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.Central
{
    public class UserClaim
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        public ClaimsSubclass Subclass { get; set; }
    }
}