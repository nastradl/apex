﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.NGRICAO
{
    public class LocBranch
    {
        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string BranchName { get; set; }

        [Required]
        [StringLength(5)]
        public string BranchCode { get; set; }
    }
}
