﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.NGRICAO
{
    public class LocControl
    {
        [StringLength(3)]
        public string ControlValue { get; set; }

        [StringLength(20)]
        public string ControlName { get; set; }

        [StringLength(60)]
        public string ControlDescription { get; set; }
    }
}
