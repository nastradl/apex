﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.NGRICAO
{
    public class LocDocInventory
    {
        [Key]
        [StringLength(18)]
        public string Chipsn { get; set; }

        [StringLength(2)]
        public string Doctype { get; set; }

        [Required]
        [StringLength(12)]
        public string Docno { get; set; }

        public DateTime? Transfertime { get; set; }

        [StringLength(5)]
        public string Branchcode { get; set; }

        [StringLength(6)]
        public string Stagecode { get; set; }

        [StringLength(50)]
        public string Boxsn { get; set; }

        public DateTime? Entrytime { get; set; }

        [StringLength(2)]
        public string Docpage { get; set; }
    }
}
