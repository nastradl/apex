﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Apex.Models.NGRICAO
{
    public class LocDocProfile
    {
        [StringLength(12)]
        public string Docno { get; set; }

        public string PassportNo { get; set; }

        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(2)]
        public string Doctype { get; set; }

        public DateTime? Expirydate { get; set; }

        public DateTime? Issuedate { get; set; }

        [StringLength(25)]
        public string Issueplace { get; set; }

        public DateTime? Printtime { get; set; }

        [StringLength(20)]
        public string Printby { get; set; }

        public DateTime? Encodetime { get; set; }

        [StringLength(20)]
        public string Encodeby { get; set; }

        public DateTime? Qctime { get; set; }

        [StringLength(20)]
        public string Qcby { get; set; }

        public DateTime? Issuetime { get; set; }

        [StringLength(20)]
        public string Issueby { get; set; }

        [StringLength(12)]
        public string Authoritycode { get; set; }

        [StringLength(50)]
        public string Acqbatch { get; set; }

        [StringLength(5)]
        public string Branchcode { get; set; }

        public int? Appreason { get; set; }

        [StringLength(6)]
        public string Stagecode { get; set; }
    }
}
