﻿using System.ComponentModel.DataAnnotations;

namespace Apex.Models.NGRICAO
{
    public class LocThirdpartyissueprofile
    {
        [Key]
        [StringLength(12)]
        public string Docno { get; set; }

        [Required]
        [StringLength(12)]
        public string Formno { get; set; }

        [Required]
        [StringLength(50)]
        public string TpName { get; set; }

        [StringLength(50)]
        public string TpPhone { get; set; }

        [StringLength(50)]
        public string TpRemarks { get; set; }
    }
}
