﻿using Apex.Models.Central;
using Apex.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Apex.Repositories
{
    public interface IAuthRepository
    {
        int GetNumberOfUsers();

        List<State> GetStates();

        List<Title> GetTitles();

        List<ApplicationUser> GetUsers();

        ApplicationUser GetUserById(int id);

        ApplicationUser GetUserByEmail(string email);

        ApplicationUser GetUserByUsername(string username);

        List<UserClaim> GetClaimsList();

        ViewAudits GetAuditTrails(int perpage, int page, string orderby, int reverse);

        ViewAudits SearchAuditTrails(SearchParam param);

        int AddClaimsToUser(int userId, string claims, int operatorId);

        int RemoveClaimsFromUser(int userId, string claims, int operatorId);

        int UpdateUserDetails(ApplicationUser user);

        int UpdateUserPassword(ApplicationUser user);

        int SetUserLockout(int userId, int operatorId, bool lockOut);

        int AddUserRequest(UserRequest request);

        int UserLoggedOut(int userId);

        int UserLoggedIn(int userId);

        int UserLoginFailed(int userId);

        bool CheckEmailRegistered(string email);

        List<UserRequest> GetUserRequests();

        Task<int> ApproveUserRequest(int userId, List<int> requestId, string tempPassword);

        int RejectUserRequest(int userId, List<int> requestId);

        int SaveError(Exception ex, string url, string userId);

        UserRequest GetUserRequest(int req);
    }
}
