﻿using Apex.Models.Central;
using Apex.Models.NGRICAO;
using Apex.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Apex.Repositories
{
    public interface IBranchRepository
    {
        List<Branch> GetBranches();

        List<AppReason> GetAppReasons();

        List<User> GetBranchUsers(string branchcode);

        Record SearchEnrolementByFormNo(string formno, string branch, int userId, int eventId);

        Record SearchEnrolementByDocNo(string docno, string branch, int userId);

        Record SearchQueryBookletInfo(string docno, string branch, int userId);

        DetailedRecord SearchEnrolementByFormNoDetailed(string formno, string branch, int userId);

        Record SearchForApprovalRecord(string formno, string branch, int userId);

        Record SearchEditBioRecord(string formno, string secondformno, string branch, int userId);

        Record SearchReverseEditBioRecord(string formno, string branch, int userId);

        Record SearchFixWrongPassportIssued(string formno, string branch, int userId);

        Record SearchCreateReissueRecord(string docno, string searchbranchcode, bool getbio, int userId);

        Record SearchBindReissueRecord(string docno, string formno, string searchbranchcode, bool getbio, int userId);

        Record SearchManageUser(string username, string branch, int userId);

        Record SearchModfyEnrolment(string formno, string searchbranchcode, int userId);

        int UpdateAfisRecord(Record record, int userId);

        Record UpdateCreateReissueRecord(Record record, int userId);

        Record UpdateBindReissueRecord(Record record, int userId);

        int UpdateBackToApproval(Record record, int userId);

        int UpdateBackToPaymentCheck(Record record, int userId);

        int UpdateBackToPerso(Record record, int userId);

        int UpdateFixWrongPassportIssued(Record record, int userId);

        int UpdateResendJobForPrinting(Record record, int userId);

        int UpdateIssuePassport(Record record, int userId);

        int UpdateSendToIssuance(Record record, int userId);

        int UpdateEnrolmentRecord(DetailedRecord record, int userId);

        int UpdateOverideBookletSequence(Record record, int userId);

        int UpdateBookletToPersoQueue(Record record, int userId);

        int UpdateEditBioRecord(Record record, int userId);

        int UpdateManageUser(Record record, int userId);

        int UpdateReverseEditBioRecord(Record record, int userId);

        int UpdateModifyEnrolmentFile(Record record, int userId);

        List<LocalQuery> SearchLocalQuery(string docno, string searchbranchcode, string gender, string firstname, string surname, string formno, int userId);

        // Mini Queries
        LocPersoProfile GetPersoProfile(string docNo, string branchCode);
    }
}
