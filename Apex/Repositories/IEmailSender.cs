﻿using System.Threading.Tasks;

namespace Apex.Repositories
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
