﻿using Apex.Enums;
using Apex.Models.NGRICAO;
using System.Threading.Tasks;

namespace Apex.Repositories
{
    public interface IEnrolProfileService
    {
        Task<LocEnrolProfile> GetEnrolProfile(string formNo);

        Task<int> UpdateEnrolProfile(LocEnrolProfile enrolProfile);

        Task<int> ChangeStageCode(EnumStageCodes from, EnumStageCodes to);
    }
}
