﻿function AuditTrails() {
    // print
    $(".printbutton").click(function () {
        var printpage = 0;
        if ($(this).hasClass("printpage"))
            printpage = 1;
        var orderby, reverse;
        if ($(".orderby.arrowdown").length) {
            orderby = $(".orderby.arrowdown").data("orderby");
            reverse = 0;
        }
        if ($(".orderby.arrowup").length) {
            orderby = $(".orderby.arrowup").data("orderby");
            reverse = 1;
        }
        var page = $("#PageNumber").val() - 1;
        var isprint = 1;
        var perpage = $("#NumberPerPage").val();


        $("#menuloader").addClass("active");

        $.ajax({
            type: "POST",
            url: "/usermanagement/GetAuditTrailPage",
            data: { orderby, reverse, page, isprint, printpage, perpage },
            dataType: "json",
            error: function () {
                $("#menuloader").removeClass("active");
            },
            success: function (result) {
                $("#menuloader").removeClass("active");

                $(".totalrows").text(result.Title);

                // populate print window and show
                $("#printwindow").html(result.page);
                window.print();
            }
        });
    });

    $(".pagination a").click(function () {
        var orderby = $("#OrderBy").val();
        var perpage = $("#NumberPerPage").val();
        var reverse;
        if ($(".orderby").hasClass("arrowdown")) {
            reverse = 1;
        } else {
            reverse = 0;
        }
        var url = $(this).attr("href");

        UpdatePage(orderby, perpage, reverse, url);
        return false;
    });

    $(".orderby").click(function () {
        var orderby = $(this).data("orderby");
        var type = $(this).data("type");
        var perpage = $("#NumberPerPage").val();
        var reverse;
        if ($(this).hasClass("arrowdown")) {
            $(this).removeClass("arrowdown").addClass("arrowup");
            reverse = 0;
        } else {
            $(".orderby").removeClass("arrowdown arrowup");
            $(this).addClass("arrowdown");
            reverse = 1;
        }

        var url = type == 2 ? "/UserManagement/GetErrorLogsPage?page=1" : "/UserManagement/GetAuditTrailPage?page=1";
        UpdatePage(orderby, perpage, reverse, url);

        return false;
    });

    $("#rangestart").calendar({
        type: "date",
        formatter: {
            date: function (date) {
                if (!date) return "";
                var day = date.getDate() + "";
                if (day.length < 2) {
                    day = "0" + day;
                }
                var month = (date.getMonth() + 1) + "";
                if (month.length < 2) {
                    month = "0" + month;
                }
                var year = date.getFullYear();
                return day + "/" + month + "/" + year;
            }
        },
        endCalendar: $("#rangeend"),
        onHide: function () {
            $("#SearchStartDate").blur();
        }
    });

    $("#rangeend").calendar({
        type: "date",
        formatter: {
            date: function (date) {
                if (!date) return "";
                var day = date.getDate() + "";
                if (day.length < 2) {
                    day = "0" + day;
                }
                var month = (date.getMonth() + 1) + "";
                if (month.length < 2) {
                    month = "0" + month;
                }
                var year = date.getFullYear();
                return day + "/" + month + "/" + year;
            }
        },
        startCalendar: $("#rangestart"),
        onHide: function () {
            $("#SearchEndDate").blur();
        }
    });

    SetDropdownsAndStuff();

    $(".ui.searchaudit")
        .checkbox().checkbox({
            onChecked: function () {
                $(".auditsearch").slideDown();
            },
            onUnchecked: function () {
                $(".auditsearch").slideUp();
            }
        });

    $("#ClearAudit").click(function () {
        $(this).closest("form").form("clear");
    });

    $("#auditSearchForm").submit(function () {
        $("#perpage").val($("#NumberPerPage").val());
        $.ajax({
            type: "POST",
            url: "/UserManagement/SearchAudit",
            dataType: "json",
            data: $(this).serialize(),
            statusCode: {
                403: function() {
                    showError(null, null, msgType.LoggedOut);
                    window.location.href = "/";
                }
            },
            error: function () {
                $(".loading").removeClass("loading");
                showError(null, null, msgType.Error);
            },
            success: function(result) {
                $(".loading").removeClass("loading");
                $("#auditSearchResults").fadeOut(function () {
                    $("tfoot").remove();
                    $(this).html(result.page);

                    $(".totalrows").text(result.total + " Rows");
                    SetDropdownsAndStuff();

                    $(this).fadeIn(function() {});
                    $("html, body").animate({ scrollTop: $(".highertable").position().top }, 1000);
                });
            }
        });

        return false;
    });

    $("#SearchAudit").click(function () {
        $(this).addClass("loading").closest("form").submit();
    });
}

function GetInnerException(id) {
    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: "/UserManagement/GetInnerException",
        dataType: "json",
        data: { id },
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            showError("Inner Exception", result.msg, 3);
        }
    });
}

function UpdatePage(orderby, perpage, reverse, url) {
    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: { orderby, perpage, reverse },
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            $("#auditSearchResults").fadeOut(function () {
                $("tfoot").remove();
                $(this).html(result.page);

                $(".totalrows").text(result.total + " Rows");
                $(this).fadeIn();
                $("body").animate({
                    scrollTop: 0
                }, 1000);
            });
        }
    });
}

function SetDropdownsAndStuff() {
    $(".numberperpage").dropdown({
        onChange: function (value) {
            var orderby = $("#OrderBy").val();
            var type = $(this).data("type");
            var perpage = value;
            var reverse;

            if ($(".orderby").hasClass("arrowdown")) {
                reverse = 1;
            } else {
                reverse = 0;
            }

            var link = type == 2 ? "GetErrorLogsPage?page=" : "GetAuditTrailPage?page=";
            var url = "/UserManagement/" + link + $("#CurrentPage").val();

            UpdatePage(orderby, perpage, reverse, url);
        }
    });
}
