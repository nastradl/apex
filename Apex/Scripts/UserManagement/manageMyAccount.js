﻿function ManageMyAccount() {
    var phoneregex = /^$|^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    $("#personalDetails")
        .form({
            className: { success: "" },
            fields: {
                username: {
                    identifier: "username",
                    rules: [
                        {
                            type: "empty",
                            prompt: "You must have a username"
                        },
                        {
                            type: "minLength[3]",
                            prompt: "Your username must be at least 3 characters"
                        },
                        {
                            type: "maxLength[15]",
                            prompt: "Your username must less than 15 characters"
                        }
                    ]
                },
                phonenumber: {
                    identifier: "phonenumber",
                    rules: [
                        {
                            type: "regExp[" + phoneregex +"]",
                            prompt: "Please enter a valid phone number"
                        }
                    ]
                }
            }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function () {
                if ($("#myusername").text() != $("#username").val()) {
                    $("#myusername").fadeOut(function () {
                        $(this).text($("#username").val()).fadeIn();
                    });
                }

                $(this).find(".success.message").show().delay(2000).fadeOut();
            },
            onFailure: function (response) {
                if (response.success != 0) {
                    if (response.error)
                        $("#personalDetails .error .header").text(response.error);

                    showError(null, null, msgType.Error);
                }
            }
        });

    $("#changepassword")
        .form({
            className: { success: "" },
            fields: {
                currentpassword: {
                    identifier: "currentpassword",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter your current password"
                        }
                    ]
                },
                newpassword: {
                    identifier: "newpassword",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Enter your new password"
                        },
                        {
                            type: "minLength[4]",
                            prompt: "Your password must be at minimum of 4 characters"
                        }
                    ]
                },
                newpasswordconfirm: {
                    identifier: "newpasswordconfirm",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Your new password and confirmation password must match"
                        }
                    ]
                }
            }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                if (response.success == 1)
                    $(this).find(".success.message").show().delay(2000).fadeOut();                    
            },
            onFailure: function (response) {
                if (response.success != 0)
                    showError(null, null, msgType.Error);
                else {
                    var errors = [response.error];
                    $(this).form("add errors", errors);
                    $(this).find(".error.message").show().delay(2000).fadeOut();
                }

            }
        });
}