﻿$(document)
    .ready(function () {
        // on menu item clicked
        $(document).on("click", "a[data-option]", function () {
            $("#sideMenuSlide .item.active").removeClass("active");
            $(this).addClass("active");

            var controller = $(this).data("section");
            var action = $(this).data("option");
            var url = "/" + controller + "/" + action;
            $("#menuloader").addClass("active");

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                error: function () {
                    $(".loader").removeClass("active");
                },
                success: function (result) {
                    $(".loader").removeClass("active");
                    $("#workspace").fadeOut(function() {
                        $(this).html(result.page);
                    
                        // after html renders, update various required scripts
                        $(".ui.dropdown")
                            .dropdown();

                        $(".ui.checkbox")
                            .checkbox();

                        AuditTrails();

                        ManageUserRequests();

                        ManageMyAccount();

                        Amu();

                        $(this).fadeIn();
                    });
                    
                    // update url
                    var loc = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
                    history.replaceState("", "PassFix", loc + "/Tools?section=" + controller + "&option=" + action);
                }
            });
        });

       
        // if page is refreshed
        var controller = getURLParameter("section");
        var action = getURLParameter("option");

        if (controller && action) {
            // close all opened accordions
            $(".accordion .title").removeClass("active");

            // activate the correct menu options
            $("#" + controller).click();

            var item = $('a[data-option="' + action + '"]');
            item.click();
            item.closest(".content").addClass("active").siblings(".title").addClass("active");
        };

        // turn off autocomplete
        $(document).on("focus", ":input", function () {
            $(this).attr("autocomplete", "off");
        });

        // formnos shouldn't be more than 12 numbers
        $(document).on("input propertychange paste", ".formno", function (e) {
            var txtbox = $(this);
            var txt = txtbox.val();
           
            if (txt.length > 12) {
                txtbox.val(txt.substring(0, 12));
                return false;
            }
        });

        //// was going to also restrict the multi-entry boxes but since we have passports and payment as well, just seems long
        //$(document).on("input propertychange paste", ".restrict", function (e) {
        //    var txtbox = $(this);
        //    var txt = txtbox.val();
        //    var len = txt.length;
        //    var lastcomma = 0;
        //    for (var i = 0; i < len; i++) {
        //        if (txt[i] == ",") {
        //            lastcomma = i;
        //            continue;
        //        }

        //        // get next comma
        //        var next = -1;
        //        for (var y = i; y < txtbox.length; y++) {
        //            if (txt[y] == ",")
        //                next = y;
        //        }

        //        if (next != -1) {
        //            // remove all till next comma
        //            var result = txt.split("");
        //            result.splice(lastcomma, next);
        //            txtbox.val(result.join(""));
        //        } else {
        //            if (i > 10) {
        //                txtbox.val(txt.substring(0, 11));
        //                return false;
        //            }
        //        }
        //    }
        //});
    });

function getURLParameter(name) {
    return decodeURIComponent(
        (new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(location.search) || [null, ""])[1].replace(/\+/g, "%20")) || null;
}
