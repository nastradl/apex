﻿using Apex.Enums;
using Apex.Extensions;
using Apex.Helpers;
using Apex.Models.Central;
using Apex.ViewModels;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Apex.Services
{
    public class AuthService
    {
        private readonly string _conn = ConfigurationManager.AppSettings["useLaptopAsBranch"] == "true" ? ConfigurationManager.ConnectionStrings["AuthLocal"].ConnectionString : 
            ConfigurationManager.ConnectionStrings["Auth"].ConnectionString;

        public int GetNumberOfUsers()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<int>("select count(*) from member").First();
            }
        }

        public List<State> GetStates()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<State>("select id, name from liststates").ToList();
            }
        }

        public MemberBranch GetMemberBranch(int memberid)
        {
            var result = new MemberBranch
            {
                MemberId = memberid
            };
            
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                var branchids = dbConn.Query<string>("select branchid from memberbranch where memberid = @memberid", new { memberid }).ToList();
                var branches = Helper.GetBranches();

                foreach (var branch in branches)
                {
                    if (branchids.Contains(branch.BranchCode))
                        result.SelectedBranches.Add(branch);
                    else
                        result.UnSelectedBranches.Add(branch);
                }
            }

            return result;
        }

        public List<Title> GetTitles()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<Title>("select id, name from listtitles").ToList();
            }
        }

        public ApplicationUser GetUserById(int id)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from member where id = @id", new { id }).FirstOrDefault();
            }
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from member where email = @email", new { email }).FirstOrDefault();
            }
        }

        public ApplicationUser GetUserByUsername(string username)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from member where username = @username", new { username }).FirstOrDefault();
            }
        }

        public List<ApplicationUser> GetUsers()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from member").ToList();
            }
        }

        public List<ApplicationUser> GetDisabledUsers()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                    return dbConn.Query<ApplicationUser>("select * where lockoutenabled = 1").ToList();
            }
        }

        public UserClaim GetClaim(int id)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserClaim>("select * from memberclaim where id = :id", new { id }).FirstOrDefault();
            }
        }

        public List<UserClaim> GetClaimsList()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserClaim>("select * from ListUserClaims").ToList();
            }
        }

        public ViewAudits GetAuditTrails(int perpage, int page, string orderby, int reverse, string printpage, string isprint)
        {
            int print = string.IsNullOrEmpty(isprint) ? 0 : 1;
            int ppage = string.IsNullOrEmpty(printpage) ? 0 : 1;
            string def = "where CONVERT(date, a.timestamp) = CONVERT(date, getdate()) ";

            var query = @"SELECT a.*, m.*, e.*
                        FROM audits a
                        INNER JOIN listauditevents e on e.id = a.eventid
                        INNER JOIN member m on m.id = a.userid ";

            if (string.IsNullOrEmpty(isprint) || isprint == "0")
            {
                query += def;
            }

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>(@"select count(1) from audits a
                        INNER JOIN listauditevents e on e.id = a.eventid
                        INNER JOIN member m on m.id = a.userid " + (string.IsNullOrEmpty(isprint) || isprint == "0" ? def : "")).First();

                List<Audit> result;
                if (reverse == 1)
                {
                    switch (orderby)
                    {
                        case "FirstName":
                            query += "order by m.firstname desc";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        case "EventName":
                            query += "order by e.name desc";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        case "TimeStamp":
                            query += "order by a.timestamp desc";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        default:
                            query += "order by e.timestamp desc";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                    }
                }
                else
                {
                    switch (orderby)
                    {
                        case "FirstName":
                            query += "order by m.firstname";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        case "EventName":
                            query += "order by e.name";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        case "TimeStamp":
                            query += "order by a.timestamp";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                        default:
                            query += "order by e.timestamp";
                            result = GetAudits(query, dbConn, page, perpage, print, ppage);
                            break;
                    }
                }

                return new ViewAudits
                {
                    Audits = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        private List<Audit> GetAudits(string query, SqlConnection dbConn, int page, int perpage, int isprint, int printpage)
        {
            var audits = dbConn.Query<Audit, ApplicationUser, AuditEvent, Audit>(
                    query,
                    (audit, user, ev) =>
                    {
                        audit.Event = new AuditEvent
                        {
                            Description = ev.Description,
                            Id = ev.Id,
                            Name = ev.Name
                        };

                        audit.User = new ApplicationUser
                        {
                            FirstName = user.FirstName,
                            Surname = user.Surname,
                            Id = user.Id
                        };

                        return audit;
                    },
                    splitOn: "id")
                .Distinct().ToList();

              var pg = audits.Skip(page * perpage).Take(perpage).ToList();

            if (isprint == 1 && printpage == 0)
                return audits;

            return pg;
        }

        public ViewAudits SearchAuditTrails(SearchParam search)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                var query = @"SELECT a.*, m.*, e.*
                        FROM audits a
                        INNER JOIN listauditevents e on e.id = a.eventid
                        INNER JOIN member m on m.id = a.userid where a.id is not null";

                var param = new DynamicParameters();

                if (!string.IsNullOrEmpty(search.UserId))
                {
                    query += " and a.userid = @userid";
                    param.Add("userid", search.UserId);
                }

                if (!string.IsNullOrEmpty(search.Actions))
                {
                    query += " and e.Name in @name";
                    param.Add("name", search.Actions.Split(','));
                }

                if (!string.IsNullOrEmpty(search.FormNo))
                {
                    query += " and a.formno = @formno";
                    param.Add("formno", search.FormNo);
                }

                if (!string.IsNullOrEmpty(search.DocNo))
                {
                    query += " and a.docno = @docno";
                    param.Add("docno", search.DocNo);
                }

                if (!string.IsNullOrEmpty(search.StartDate))
                {
                    var startdate = DateTime.Parse(search.StartDate);
                    query += " and a.timestamp >= @startdate";
                    param.Add("startdate", startdate);
                }

                if (!string.IsNullOrEmpty(search.EndDate))
                {
                    var enddate = DateTime.Parse(search.EndDate).AddDays(1);
                    query += " and a.timestamp < @enddate";
                    param.Add("enddate", enddate);
                }

                var audits = dbConn.Query<Audit, ApplicationUser, AuditEvent, Audit>(
                        query + " order by a.timestamp desc",
                        (audit, user, ev) =>
                        {
                            audit.Event = new AuditEvent
                            {
                                Description = ev.Description,
                                Id = ev.Id,
                                Name = ev.Name
                            };

                            audit.User = new ApplicationUser
                            {
                                FirstName = user.FirstName,
                                Surname = user.Surname,
                                Id = user.Id
                            };

                            return audit;
                        },
                        param,
                        splitOn: "id")
                    .Distinct().ToList();

                if (search.IsPrint == 1 && search.PrintPage == 0)
                {
                    search.Page = 0;
                    search.PerPage = audits.Count;
                }

                var page = audits.Skip(search.Page * search.PerPage).Take(search.PerPage).ToList();

                if (search.IsPrint == 1 && search.PrintPage == 0)
                    return new ViewAudits
                    {
                        Audits = audits,
                        CurrentPage = 1,
                        NumberOfPages = 1,
                        NumberPerPage = audits.Count,
                        Total = audits.Count
                    };

                return new ViewAudits
                {
                    Audits = page,
                    CurrentPage = 1,
                    NumberOfPages = audits.Count / search.PerPage,
                    NumberPerPage = search.PerPage,
                    Total = audits.Count
                };
            }
        }
        
        public int UpdateUserDetails(ApplicationUser user)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                var appuser = GetUserById(user.Id);

                // if its a different username, confirm it is unique
                if (!string.Equals(appuser.UserName, user.UserName, StringComparison.CurrentCultureIgnoreCase))
                {
                    var exists = GetUserByUsername(user.UserName.ToLower());

                    if (exists != null)
                        return -1;
                }
                
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PersonalDetailsUpdated,
                    Description = EnumAuditEvents.PersonalDetailsUpdated.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = user.Id
                });

                return dbConn.Execute("update member set username = @username, phonenumber = @phonenumber where id = @id", new
                {
                    user.UserName,
                    user.PhoneNumber,
                    user.Id
                });
            }
        }

        public int UpdateUserPassword(ApplicationUser user)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PasswordChanged,
                    Description = EnumAuditEvents.PasswordChanged.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = user.Id
                });

                return 1;
            }
        }

        public int SetUserLockout(int userId, int operatorId, bool lockOut)
        {
            var appuser = GetUserById(userId);

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                
                if (lockOut)
                {
                    dbConn.Execute("update member set LockoutEnabled = 1 where id = @id", new
                    {
                        id = userId
                    });

                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.DisableUser,
                        Description = EnumAuditEvents.DisableUser.GetEnumDescription() + "- User: " + appuser.GetFullName(),
                        TimeStamp = DateTime.Now,
                        UserId = operatorId
                    });
                }
                else
                {
                    dbConn.Execute("update member set LockoutEnabled = 0 where id = @id", new
                    {
                        id = userId
                    });

                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.EnableUser,
                        Description = EnumAuditEvents.EnableUser.GetEnumDescription() + "- User: " + appuser.GetFullName(),
                        TimeStamp = DateTime.Now,
                        UserId = operatorId
                    });
                }
                

                return 1;
            }
        }

        public int AddUserRequest(UserRequest request)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                dbConn.Execute("insert into UserRequests (datecreated, emailaddress, firstname, message, phonenumber, surname) values (@datecreated, @emailaddress, @firstname, @message, @phonenumber, @surname)",
                    new
                    {
                        request.DateCreated,
                        request.EmailAddress,
                        request.FirstName,
                        request.Message,
                        request.PhoneNumber,
                        request.Surname
                    });

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.RequestAdded,
                    Description = EnumAuditEvents.RequestAdded.GetEnumDescription(),
                    TimeStamp = DateTime.Now
                });

                return 1;
            }
        }

        public int AddClaimsToUser(int userId, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim {Action = value, Controller = type });
            }

            // confirm user doesn't have these claims (yeah, shouldn't be necessary but getting some funny double postings)
            var userclaims = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().GetClaims(userId);
            var exists = claimslist.Where(x => userclaims.Any(y => y.Value == x.Action && y.Type == x.Controller));
            var newlist = claimslist.Except(exists).ToList();

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in newlist)
                {
                    dbConn.Execute(
                        "insert into memberclaim (memberid, claimtype, claimvalue) values (@memberid, @claimtype, @claimvalue)",
                        new
                        {
                            memberid = userId,
                            claimtype = claim.Controller,
                            claimvalue = claim.Action
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeAssigned,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId,
                    ToUserId = userId
                });
            }

            return 1;
        }

        public int RemoveClaimsFromUser(int userId, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim { Action = value, Controller = type });
            }

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in claimslist)
                {
                    dbConn.Execute(
                        "delete from memberclaim where claimtype = @claimtype and claimvalue = @claimvalue and memberid = @memberid",
                        new
                        {
                            claimtype = claim.Controller,
                            claimvalue = claim.Action,
                            memberid = userId
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeRevoked,
                    Description = EnumAuditEvents.PrivilegeRevoked.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId,
                    ToUserId = userId
                });
            }

            return 1;
        }

        public int UserLoggedOut(int userId)
        {
            AddToAudit(new Audit
            {
                EventId = (int)EnumAuditEvents.LogOut,
                Description = EnumAuditEvents.LogOut.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            return 1;
        }

        public int UserLoggedIn(int userId)
        {
            AddToAudit(new Audit
            {
                EventId = (int)EnumAuditEvents.LoginSuccess,
                Description = EnumAuditEvents.LoginSuccess.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            return 1;
        }

        public int UserLoginFailed(int userId)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Execute("insert into Audits (eventid, timestamp, userid) values (@eventid, @timestamp, @userid)",
                    new
                    {
                        EventId = (int)EnumAuditEvents.LoginFailed,
                        Description = EnumAuditEvents.LoginSuccess.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
            }
        }

        public bool CheckEmailRegistered(string email)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                var registered = dbConn.Query<ApplicationUser>("select id from member where email = @email", new { email }).FirstOrDefault() != null;

                var requested = dbConn.Query<ApplicationUser>("select id from userrequests where emailaddress = @email and approved is null", new { email }).FirstOrDefault() != null;

                return registered || requested;
            }
        }

        public UserRequest GetUserRequest(int id)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserRequest>("select * from userrequests where id = @id", new { id }).FirstOrDefault();
            }
        }

        public List<UserRequest> GetUserRequests()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserRequest>("select * from userrequests where approved is null").ToList();
            }
        }

        public async Task<int> ApproveUserRequest(int userId, List<int> requestId, string tempPassword)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                foreach (var id in requestId)
                {

                    var request = dbConn.Query<UserRequest>("select * from userrequests where id = @id", new {id})
                        .FirstOrDefault();

                    if (request == null)
                        return 0;

                    // If email already registered then don't continue (shouldn't be possible but doesn't hurt to check)
                    if (dbConn.Query<ApplicationUser>("select id from member where email = @email", new { email = request.EmailAddress }).Any())
                        continue;

                    // check if username is already in use
                    string username = GetUniqueUsername(request.EmailAddress.Split('@')[0]);

                    // create a new user from request
                    var user = new ApplicationUser
                    {
                        FirstName = request.FirstName,
                        Surname = request.Surname,
                        PhoneNumber = request.PhoneNumber,
                        Email = request.EmailAddress,
                        UserName = username.ToLower(),
                        DateCreated = DateTime.Now,
                        SecurityStamp = Guid.NewGuid().ToString("D")
                    };
                    
                    var result = await HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().CreateAsync(user, tempPassword);

                    if (result.Succeeded)
                    {
                        // update the request
                        dbConn.Execute("update userrequests set approved = 1, userid = @userid where id = @id", new { id, userid = user.Id });

                        // Audit
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.RequestApproved,
                            Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });

                        return 1;
                    }
                }

                return 0;
            }
        }
        
        public int RejectUserRequest(int userId, List<int> requestId)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var id in requestId)
                {
                    dbConn.Execute("update userrequests set approved = 0 where id = @id", new { id });

                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.RequestDenied,
                        Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
                }
            }

            return 1;
        }

        public int AddClaimsToSubclass(int id, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim { Action = value, Controller = type });
            }

            var subclass = GetSubClass(id);

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in claimslist)
                {
                    dbConn.Execute(
                        "update listuserclaims set subclass_id = @subclassid where action = @action and controller = @controller",
                        new
                        {
                            action = claim.Action,
                            controller = claim.Controller,
                            subclassid = id
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeAssignedSubclass,
                    Description = EnumAuditEvents.PrivilegeAssignedSubclass.GetEnumDescription() + $" {subclass.SubclassName} - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return 1;
        }

        public int RemoveClaimsFromSubclass(int id, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim { Action = value, Controller = type });
            }

            var subclass = GetSubClass(id);

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in claimslist)
                {
                    dbConn.Execute(
                        "update listuserclaims set subclass_id = NULL where action = @action and controller = @controller",
                        new
                        {
                            action = claim.Action,
                            controller = claim.Controller,
                            subclassid = id
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeRevokedSubclass,
                    Description = EnumAuditEvents.PrivilegeRevokedSubclass.GetEnumDescription() + $" {subclass.SubclassName} - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return 1;
        }

        public int AddBranchToUser(int id, string branches, int operatorId)
        {
            var commavalues = branches.Split(',').ToList();
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var branch in commavalues)
                {
                    dbConn.Execute(
                        "insert into memberbranch (branchid, memberid) values (@branchid, @memberid)",
                        new
                        {
                            branchid = branch,
                            memberid = id
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);
            var user = GetUserById(id);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.BranchAssignedToUser,
                    Description = EnumAuditEvents.BranchAssignedToUser.GetEnumDescription() + $" {string.Join(", ", part)} - {user.GetFullName()}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId,
                    ToUserId = id
                });
            }

            return 1;
        }

        public int RemoveBranchFromUser(int id, string branches, int operatorId)
        {
            var commavalues = branches.Split(',').ToList();
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                foreach (var branch in commavalues)
                {
                    dbConn.Execute(
                        "delete from memberbranch where branchid = @branchid and memberid = @memberid",
                        new
                        {
                            branchid = branch,
                            memberid = id
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);
            var user = GetUserById(id);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.BranchRemovedFromUser,
                    Description = EnumAuditEvents.BranchRemovedFromUser.GetEnumDescription() + $" {string.Join(", ", part)} - {user.GetFullName()}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId,
                    ToUserId = id
                });
            }

            return 1;
        }

        public int SaveError(Exception ex, string url, string userId)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                dbConn.Execute("insert into Errorlogs (innerexception, message, datecreated, type, url, userid) values (@innerexception, @message, @datecreated, @type, @url, @userid)",
                    new
                    {
                        userId,
                        datecreated = DateTime.Now,
                        innerexception = ex.ToString(),
                        ex.Message,
                        type = ex.GetType().ToString(),
                        url
                    });

                return 1;
            }
        }

        public List<ClaimsSubclass> GetSubClasses()
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                var result = new List<ClaimsSubclass>();

                dbConn.Query<ClaimsSubclass, UserClaim, ClaimsSubclass>(@"
                    SELECT u.*, l.*
                    FROM UserClaimsSubclass u
                    INNER JOIN ListUserClaims l ON l.Subclass_Id = u.Id 
                    ",
                        (userclaimssubclass, listuserclaim) =>
                        {
                            if (userclaimssubclass.Claims == null)
                                userclaimssubclass.Claims = new List<UserClaim>();

                            var subclass = result.FirstOrDefault(x => x.Id == userclaimssubclass.Id);
                            if (subclass == null)
                            {
                                subclass = userclaimssubclass;
                                result.Add(userclaimssubclass);
                            }
                                

                            subclass.Claims.Add(listuserclaim);
                            return userclaimssubclass;
                        });

                return result;
            }
        }

        public ClaimsSubclass GetSubClass(int id)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                var result = new List<ClaimsSubclass>();

                dbConn.Query<ClaimsSubclass, UserClaim, ClaimsSubclass>(@"
                    SELECT u.*, l.*
                    FROM UserClaimsSubclass u
                    INNER JOIN ListUserClaims l ON l.Subclass_Id = u.Id 
                    where u.id = @id
                    ",
                    (userclaimssubclass, listuserclaim) =>
                    {
                        if (userclaimssubclass.Claims == null)
                            userclaimssubclass.Claims = new List<UserClaim>();

                        var subclass = result.FirstOrDefault(x => x.Id == userclaimssubclass.Id);
                        if (subclass == null)
                        {
                            subclass = userclaimssubclass;
                            result.Add(userclaimssubclass);
                        }


                        subclass.Claims.Add(listuserclaim);
                        return userclaimssubclass;
                    }, 
                    new { id}
                    );

                return result.FirstOrDefault();
            }
        }

        public ViewErrors GetErrorLogs(int perpage, int page, string orderby, int reverse)
        {
            var query = @"SELECT e.*, m.*
                        FROM errorlogs e
                        INNER JOIN member m on m.id = e.userid ";

            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>("select count(id) from errorlogs").First();

                List<ErrorLog> result;
                if (reverse == 1)
                {
                    switch (orderby)
                    {
                        case "User":
                            query += "order by m.firstname desc";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        case "Type":
                            query += "order by e.type desc";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.datecreated desc";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                    }
                }
                else
                {
                    switch (orderby)
                    {
                        case "User":
                            query += "order by m.firstname";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        case "Type":
                            query += "order by e.type";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.datecreated";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                    }
                }

                return new ViewErrors
                {
                    ErrorLogs = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        public ErrorLog GetErrorLog(int id)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ErrorLog>("select * from errorlogs where id = @id", new { id }).FirstOrDefault();
            }
        }

        private List<ErrorLog> GetErrorList(string query, SqlConnection dbConn, int page, int perpage)
        {
            return dbConn.Query<ErrorLog, ApplicationUser, ErrorLog>(
                    query,
                    (error, user) =>
                    {
                        error.ApplicationUser = new ApplicationUser
                        {
                            FirstName = user.FirstName,
                            Surname = user.Surname,
                            Id = user.Id
                        };

                        return error;
                    },
                    splitOn: "id")
                .Distinct()
                .Skip(page * perpage)
                .Take(perpage).ToList();
        }

        private void AddToAudit(Audit audit)
        {
            using (var dbConn = new SqlConnection(_conn))
            {
                dbConn.Open();
                dbConn.Execute("insert into Audits (eventid, timestamp, userid, ToUserId) values (@eventid, @timestamp, @userid, @ToUserId)",
                    new
                    {
                        audit.EventId,
                        audit.TimeStamp,
                        audit.UserId,
                        audit.ToUserId
                    });
            }
        }

        private string GetUniqueUsername(string username)
        {
            while (true)
            {
                // recursively check till no user exists
                using (var dbConn = new SqlConnection(_conn))
                {
                    dbConn.Open();

                    var duplicate = dbConn.Query<int>("select count(1) from member where username = @username", new { username }).First();

                    if (duplicate == 0)
                        return username;

                    username = username + duplicate;
                }
            }
        }
    }
}
