﻿using Apex.Enums;
using Apex.Extensions;
using Apex.Helpers;
using Apex.Models.Central;
using Apex.Models.NGRICAO;
using Apex.ViewModels;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace Apex.Services
{
    public class BranchService
    {
        private readonly string _auth = ConfigurationManager.AppSettings["useLaptopAsCentral"] == "true" ? ConfigurationManager.ConnectionStrings["AuthLocal"].ConnectionString
            : ConfigurationManager.ConnectionStrings["Auth"].ConnectionString;
        private readonly string _passport = ConfigurationManager.ConnectionStrings["OraclePassport"].ConnectionString;


        public List<CheckDate> GetDateChanged(int userId)
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();

                var audits = dbConn.Query<Audit>("select * from audits where eventid = 17").ToList();

                var dic = new Dictionary<int, string>();
                var dates = new List<CheckDate>();
                foreach (var audit in audits)
                {
                    if (!string.IsNullOrEmpty(audit.Description))
                    {
                        var formno = Regex.Replace(audit.Description, ".*FormNo: ", "");
                        dic.Add(audit.Id, formno);
                    }
                }

                var path = "C:/Downloads/info2.txt";
                var checkdate = new CheckDate();
                var lastbranch = "";
                bool skipbranch = false;

                foreach (var pair in dic)
                {
                    try
                    {
                        checkdate = new CheckDate();
                        var formno = pair.Value;
                        checkdate.Formno = formno;

                        var branchcon = GetConnectionString(formno.Substring(0, 3), userId);

                        if (branchcon.BranchCode == lastbranch && skipbranch)
                            continue;

                        skipbranch = false;

                        using (var conn = new SqlConnection(branchcon.conn))
                        {
                            lastbranch = branchcon.BranchCode;
                            conn.Open();

                            var olddocno = conn.Query<string>("select olddocno from loc_enrolprofile where formno = @formno",
                                new { formno }).SingleOrDefault();

                            checkdate.NewDate = conn.Query<DateTime>("select birthdate from loc_docholdermainprofile where formno = @formno",
                                new { formno }).SingleOrDefault();

                            if (!string.IsNullOrEmpty(olddocno))
                            {
                                var record = GetPassportFromCentralDocno(olddocno, false, 1);
                                checkdate.PreviousDate = record.Passport.LocDocHolderMainProfile.Birthdate;
                            }
                            else
                                continue;
                        }

                        dates.Add(checkdate);

                        if (checkdate.PreviousDate != null && checkdate.PreviousDate.Value.Day == checkdate.NewDate.Value.Month && checkdate.PreviousDate.Value.Month == checkdate.NewDate.Value.Day)
                        {
                            using (var tw = new StreamWriter(path, true))
                            {
                                var diff = checkdate.PreviousDate == checkdate.NewDate ? "NO" : "YES";
                                tw.WriteLine($"Formno: {checkdate.Formno} | Branchdate: {checkdate.NewDate} | Centraldate: {checkdate.PreviousDate} | Different: {diff}");
                                tw.Close();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        skipbranch = true;
                    }

                }

                return dates;
            }
        }

        public void UpdateOldFormNo()
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();

                var audits = dbConn.Query<Audit>("select * from audits").ToList();

                foreach (var audit in audits)
                {
                    if (!string.IsNullOrEmpty(audit.Description))
                    {
                        try
                        {
                            //if (audit.FormNo.Any(char.IsLetter))
                            //    dbConn.Execute("update audits set formno = NULL where id = @id",
                            //        new { id = audit.Id });

                            if (audit.Description.ToLower().Contains("oldformno: "))
                            {
                                var formno = Regex.Replace(audit.Description, ".*OldFormNo: ", "",
                                    RegexOptions.IgnoreCase);
                                if (formno.Length > 12)
                                    formno = formno.Substring(0, 12);

                                if (formno.Any(char.IsLetter))
                                    formno = "";

                                dbConn.Execute("update audits set oldformno = @formno where id = @id",
                                    new { formno, id = audit.Id });
                            }
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                        }
                    }
                }
            }
        }

        public void SetFormNoAudit(int type)
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();

                var audits = dbConn.Query<Audit>("select * from audits").ToList();

                foreach (var audit in audits)
                {
                    if (!string.IsNullOrEmpty(audit.Description))
                    {
                        try
                        {
                            if (type == 1)
                            {
                                if (audit.Description.ToLower().Contains("newformno: "))
                                {
                                    var formno = Regex.Replace(audit.Description, ".*NewFormNo: ", "",
                                        RegexOptions.IgnoreCase);
                                    if (formno.Length > 12)
                                        formno = formno.Substring(0, 12);

                                    if (!formno.Any(char.IsLetter))
                                        dbConn.Execute("update audits set formno = @formno where id = @id",
                                        new {formno, id = audit.Id});
                                }
                            }
                            else
                            {
                                if (audit.Description.ToLower().Contains("docno: "))
                                {
                                    var docno = Regex.Replace(audit.Description, ".*DocNo: ", "",
                                        RegexOptions.IgnoreCase);
                                    if (docno.Length > 9)
                                        docno = docno.Substring(0, 9);

                                    dbConn.Execute("update audits set docno = @docno where id = @id",
                                        new {docno, id = audit.Id});
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                        }
                    }
                }
            }
        }

        public void GetNullOthername(bool fix, int userId)
        {
            var branches = Helper.GetBranches();

            var path = "C:/Downloads/info2.txt";
                
            foreach (var branch in branches)
            {
                try
                {
                    var branchcon = GetConnectionString(branch.BranchCode, userId);

                    using (var conn = new SqlConnection(branchcon.conn))
                    {
                        conn.Open();
                       
                        var forms = conn.Query<string>("select formno from loc_docholdercustomprofile where othername1 is null").ToList();

                        using (var tw = new StreamWriter(path, true))
                        {
                            foreach (var form in forms)
                            {
                                tw.WriteLine(form);
                            }
                               
                            tw.Close();
                        }

                        if (fix && forms.Count != 0)
                        {
                            if (branch.BranchCode != "317")
                            {
                                conn.Execute("update loc_docholdercustomprofile set othername1 = '' where formno in @formno", new
                                {
                                    formno = forms
                                });

                                using (var tw = new StreamWriter(path, true))
                                {
                                    tw.WriteLine(string.Join(", ", forms) + ": Fixed");
                                    tw.Close();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // say you couldn't reach this branch
                    using (var tw = new StreamWriter(path, true))
                    {
                        tw.WriteLine("Could not connect to " + branch.BranchName);
                        tw.Close();
                    }
                }

            }
        }

        public List<Branch> GetBranches()
        {
            using (var client = new WebClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"/WebInvestigatorService.svc/getbranches";
                var response = client.DownloadString(serviceurl + contracturl);

                return JsonConvert.DeserializeObject<List<Branch>>(response);
                
            }
        }

        public List<User> GetBranchUsers(string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return null;

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                return dbConn.Query<User>("select * from [User] order by loginname").ToList();
            }
        }

        public List<AppReason> GetAppReasons()
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();
                return dbConn.Query<AppReason>("select * from listappreasons").ToList();
            }
        }

        public List<string> GetReissueReasons()
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();
                return dbConn.Query<string>("select name from listreissuereasons").ToList();
            }
        }

        public async Task<List<ProcessingOffice>> GetPaymentOffices()
        {
            using (var client = new HttpClient())
            {
                // Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + "/api/payment/getProcessingOffices";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<ProcessingOffice>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<List<ProcessingState>> GetPaymentStates()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + "/api/payment/getProcessingStates";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<ProcessingState>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<Record> GetPassportFromCentralFormno(string formno, bool withbio, int userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"WebInvestigatorService.svc/getpassportdetails/{formno}/{withbio}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchCreateReissueRecord,
                    Description = EnumAuditEvents.SearchCreateReissueRecord.GetEnumDescription() + $" - Formno: {formno}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = formno
                });

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var record = new Record
                        {
                            Passport = JsonConvert.DeserializeObject<LocPassport>(response.Content.ReadAsStringAsync()
                                .Result)
                        };
                        return record;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public Record GetPassportFromCentralDocno(string docno, bool withbio, int userId)
        {
            // get details from central
            using (var client = new WebClient())
            {
                try
                {
                    //Passing service base url  
                    var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                    var contracturl = $"/WebInvestigatorService.svc/getpassportdetailsdocno/{docno.ToUpper()}/{withbio}";
                    var response = client.DownloadString(serviceurl + contracturl);

                    // add audit
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.SearchCreateReissueRecord,
                        Description = EnumAuditEvents.SearchCreateReissueRecord.GetEnumDescription() + $" - Docno: {docno}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        DocNo = docno
                    });

                    var record = new Record
                    {
                        Passport = JsonConvert.DeserializeObject<LocPassport>(response)
                    };

                    return record;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public Record SearchEnrolementByFormNo(string formno, string branchcode, int userId, int eventId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();
                var query = @"SELECT e.*, m.*, c.*, p.*
                    FROM loc_enrolprofile e
                    LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                    LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno
                    LEFT JOIN loc_persoProfile p on e.formno = p.formno
                    where e.formno = @formno";

                // check for docprofile
                var docprofiles = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno order by encodetime desc", new { formno }).ToList();

                // if there is a docprofile, get the passport number
                string passportNo = string.Empty;
                string doctype = string.Empty;
                string docstagecode = string.Empty;

                if (docprofiles.Count != 0)
                {
                    passportNo = docprofiles[0].Docno;
                    doctype = docprofiles[0].Doctype;
                    docstagecode = docprofiles[0].Stagecode;
                }

                Record record = null;
                dbConn
                    .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile, LocPersoProfile,
                        LocEnrolProfile>(
                        query,
                        (enroleProfile, docHolderMainpProfile, docHolderCustomProfile, persoProfile) =>
                        {
                            record = new Record
                            {
                                FirstName = docHolderMainpProfile.Firstname,
                                Surname = docHolderMainpProfile.Surname,
                                MiddleName = docHolderCustomProfile?.Othername1,
                                FormNo = enroleProfile.Formno,
                                StageCode = enroleProfile.Stagecode,
                                BranchCode = branch.BranchCode,
                                PassportNo = passportNo,
                                PassportType = doctype,
                                LocationId = enroleProfile.Enrollocationid,
                                PersoLocationId = persoProfile?.Persolocationid,
                                DocProfileStageCode = docstagecode,
                                Remark = enroleProfile.Remarks,
                                PersoProfileStageCode = persoProfile?.Stagecode,
                                DocProfiles = docprofiles?.Count > 1 ? docprofiles : null
                            };

                            return enroleProfile;
                        },
                        new { formno },
                        splitOn: "formno");

                AddToAudit(new Audit
                {
                    EventId = eventId,
                    Description = ((EnumAuditEvents)eventId).GetEnumDescription() +
                                  $" - FormNo: {formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return record;
            }
        }

        public DetailedRecord SearchEnrolementByFormNoDetailed(string formno, string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new DetailedRecord
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var query = @"SELECT e.*, m.*, c.*, ph.*
                    FROM loc_enrolprofile e
                    LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                    LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno
                    LEFT JOIN loc_paymentHistory ph on e.formno = ph.formno
                    where e.formno = @formno";

                DetailedRecord record = null;
                dbConn
                    .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile, LocPaymentHistory,
                        LocEnrolProfile>(
                        query,
                        (enroleProfile, docHolderMainpProfile, docHolderCustomProfile, paymentHistory) =>
                        {
                            record = new DetailedRecord
                            {
                                FirstName = docHolderMainpProfile.Firstname,
                                Surname = docHolderMainpProfile.Surname,
                                MiddleName = docHolderCustomProfile.Othername1,
                                FormNo = enroleProfile.Formno,
                                StageCode = enroleProfile.Stagecode,
                                BranchCode = branch.BranchCode,
                                ApplicationId = paymentHistory?.Appid,
                                ReferenceId = paymentHistory?.Refno,
                                Appreason = enroleProfile.Appreason,
                                DateOfBirth = docHolderMainpProfile.Birthdate,
                                DocPage = enroleProfile.Docpage,
                                DocType = enroleProfile.Doctype,
                                Remarks = enroleProfile.Remarks,
                                PersonalNo = docHolderMainpProfile.Personalno,
                                Nofinger = enroleProfile.Nofinger == 1,
                                Title = docHolderCustomProfile.Title,
                                OriginState = docHolderCustomProfile.Originstate,
                                BirthState = docHolderMainpProfile.Birthstate,
                                Sex = docHolderMainpProfile.Sex,
                                EnrolLocationId = enroleProfile.Enrollocationid.ToString(),
                                BirthTown = docHolderMainpProfile.Birthtown
                            };

                            return enroleProfile;
                        },
                        new { formno },
                        splitOn: "formno");

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return record;
            }
        }

        public Record SearchEnrolementByDocNo(string docno, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();
                var docprofile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where docno = @docno", new { docno }).SingleOrDefault();

                return docprofile == null ? null : SearchEnrolementByFormNo(docprofile.Formno, branchcode, userId, (int)EnumAuditEvents.SearchEnrolProfile);
            }
        }

        public Record SearchQueryBookletInfo(string docno, string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var docinventory = dbConn.Query<LocDocInventory>("select * from loc_docinventory where docno = @docno", new { docno }).SingleOrDefault();

                if (docinventory == null)
                    return null;

                var result = new Record
                {
                    PassportNo = docinventory.Docno,
                    BoxSn = docinventory.Boxsn,
                    ChipSn = docinventory.Chipsn,
                    StageCode = docinventory.Stagecode,
                    BranchCode = branch.BranchCode
                };

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - DocNo: {docno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = docno
                });

                return result;
            }
        }

        public Record SearchForApprovalRecord(string formno, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();
                var query = @"SELECT e.*, m.*, c.*
                    FROM loc_enrolprofile e
                    LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                    LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno
                    where e.formno = @formno";

                Record record = null;
                dbConn
                    .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile,
                        LocEnrolProfile>(
                        query,
                        (enroleProfile, docHolderMainpProfile, docHolderCustomProfile) =>
                        {
                            record = new Record
                            {
                                FirstName = docHolderMainpProfile.Firstname,
                                Surname = docHolderMainpProfile.Surname,
                                MiddleName = docHolderCustomProfile.Othername1,
                                FormNo = enroleProfile.Formno,
                                StageCode = enroleProfile.Stagecode,
                                BranchCode = branch.BranchCode,
                                LocationId = enroleProfile.Enrollocationid
                            };

                            return enroleProfile;
                        },
                        new { formno },
                        splitOn: "formno");


                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });
                
                // if docprofile or persoprofile exist, return error
                var hasDocprofile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { formno }).FirstOrDefault();
                var hasPerso = dbConn.Query<LocDocProfile>("select * from loc_persoprofile where formno = @formno", new { formno }).FirstOrDefault();

                if (hasDocprofile != null)
                    record.Error = "Cannot alter this record as it has a DocProfile";

                if (hasPerso != null)
                {
                    // There is a special case where an incomplete persoprofile is created and needs to be deleted check for it 
                    if (hasPerso.Docno == null)
                    {
                        dbConn.Execute("delete from loc_persoProfile where formno = @formno", new {formno});
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.DeleteIncompletePerso,
                            Description = EnumAuditEvents.DeleteIncompletePerso.GetEnumDescription() + $" - FormNo: {formno}, Branch: { branch.BranchName }",
                            TimeStamp = DateTime.Now,
                            UserId = userId,
                            FormNo = formno
                        });
                    }
                    else
                    {
                        if (hasDocprofile != null)
                            record.Error += " and a PersoProfile";
                        else
                            record.Error = "Cannot alter this record as it has a PersoProfile";
                    }
                }

                if (!string.IsNullOrEmpty(record.Error))
                    return record;

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: { branch.BranchName }",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return record;
            }
        }

        public Record SearchEditBioRecord(string formno, string secondformno, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var bioProfile1 = dbConn.Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @formno", new { formno }).SingleOrDefault();
                var bioProfile2 = dbConn.Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @secondformno", new { secondformno }).SingleOrDefault();

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEditBioRecord,
                    Description = EnumAuditEvents.SearchEditBioRecord.GetEnumDescription() + $" - FormNoOld: {formno}, FormNoNew: {secondformno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return new Record
                {
                    FaceCurrent = bioProfile1?.Faceimage,
                    FaceReplace = bioProfile2?.Faceimage,
                    SignatureCurrent = bioProfile1?.Signimage,
                    SignatureReplace = bioProfile2?.Signimage,
                    ShowRevert = false
                };

                
            }
        }

        public Record SearchReverseEditBioRecord(string formno, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var central = new SqlConnection(_auth))
            {
                central.Open();

                // check if any record to revert to
                var revert = central
                    .Query<EditBioRecordStore>(
                        "select * from EditBioRecordStorage where formno = @formno order by datechanged desc",
                        new {formno}).FirstOrDefault();

                if (revert == null)
                    return null;

                using (var dbConn = new SqlConnection(branch.conn))
                {
                    dbConn.Open();

                    var bioProfile2 = dbConn
                        .Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @formno",
                            new {formno}).SingleOrDefault();

                    if (bioProfile2 == null)
                        return null;

                    // add audit
                    AddToAudit(new Audit
                    {
                        EventId = (int) EnumAuditEvents.SearchReverseEditBioRecord,
                        Description = EnumAuditEvents.SearchReverseEditBioRecord.GetEnumDescription() +
                                      $" - FormNoOld: {formno}, Branch: {branch.BranchName}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = formno
                    });

                    var same = revert.Face != null && revert.Signature != null &&  bioProfile2.Faceimage.SequenceEqual(revert.Face) &&
                               bioProfile2.Signimage.SequenceEqual(revert.Signature);
                    if (same)
                        return null;

                    return new Record
                    {
                        FaceCurrent = revert.Face,
                        FaceReplace = bioProfile2?.Faceimage,
                        SignatureCurrent = revert.Signature,
                        SignatureReplace = bioProfile2?.Signimage,
                        ShowRevert = true
                    };
                }
            }
        }

        public Record SearchFixWrongPassportIssued(string formno, string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var query = @"SELECT e.*, m.*, c.*, p.*
                    FROM loc_enrolprofile e
                    LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                    LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno
                    LEFT JOIN loc_persoProfile p on e.formno = p.formno
                    where e.formno = @formno";

                // check for docprofile
                var docprofiles = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { formno }).ToList();

                // if there is a docprofile, get the passport number
                string doctype = string.Empty;
                string docstagecode = string.Empty;

                // There must be 2 docprofiles
                if (docprofiles.Count != 2)
                {
                    return new Record
                    {
                        Error = "Only one docprofile found. This process requires at least 2."
                    };
                }

                if (docprofiles.Count != 0)
                {
                    doctype = docprofiles[0].Doctype;
                    docstagecode = docprofiles[0].Stagecode;
                }

                string stagecode1 = "";
                string stagecode2 = "";
                string booklet1 = "";
                string booklet2 = "";

                foreach (var profile in docprofiles)
                {
                    // check if this passport has a perso profile
                    var perso = dbConn.Query<LocDocProfile>("select * from loc_persoprofile where docno = @docno", new { profile.Docno }).FirstOrDefault();

                    if (perso == null)
                    {
                        booklet2 = profile.Docno;
                        stagecode2 = profile.Stagecode;
                    }
                    else
                    {
                        stagecode1 = profile.Stagecode;
                        booklet1 = profile.Docno;
                    }
                }

                Record record = null;
                dbConn
                    .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile, LocPersoProfile,
                        LocEnrolProfile>(
                        query,
                        (enroleProfile, docHolderMainpProfile, docHolderCustomProfile, persoProfile) =>
                        {
                            record = new Record
                            {
                                FirstName = docHolderMainpProfile.Firstname,
                                Surname = docHolderMainpProfile.Surname,
                                MiddleName = docHolderCustomProfile?.Othername1,
                                FormNo = enroleProfile.Formno,
                                StageCode = enroleProfile.Stagecode,
                                BranchCode = branch.BranchCode,
                                PassportNo = booklet2,
                                PassportType = doctype,
                                DocNoTwo = booklet1,
                                DocTwoStageCode = stagecode1,
                                DocStageCode2 = stagecode2,
                                PersoLocationId = persoProfile.Persolocationid,
                                DocProfileStageCode = docstagecode,
                                PersoProfileStageCode = persoProfile?.Stagecode,
                                DocProfiles = docprofiles?.Count > 1 ? docprofiles : null
                            };

                            return enroleProfile;
                        },
                        new { formno },
                        splitOn: "formno");

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchFixWrongPassportIssued,
                    Description = EnumAuditEvents.SearchFixWrongPassportIssued.GetEnumDescription() + $" - FormNo: {formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return record;
            }
        }

        public Record SearchCreateReissueRecord(string docno, string branchcode, bool getbio, int userId)
        {
            return GetPassportFromCentralDocno(docno, getbio, userId);
        }

        public Record SearchBindReissueRecord(string docno, string formno, string branchcode, bool getbio, int userId)
        {
            // get details from central
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"WebInvestigatorService.svc/getpassportdetailsdocno/{docno}/{getbio}";
                client.BaseAddress = new Uri(serviceurl);
                var response = client.GetAsync(contracturl).Result;

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchBindReissueRecord,
                    Description = EnumAuditEvents.SearchBindReissueRecord.GetEnumDescription() + $" - DocNo: {docno}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno,
                    DocNo = docno
                });

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var record = new Record
                        {
                            Passport = JsonConvert.DeserializeObject<LocPassport>(response.Content.ReadAsStringAsync()
                                .Result)
                        };

                        var branch = GetConnectionString(branchcode, userId);

                        if (branch == null)
                            return new Record
                            {
                                Error = "Either branch not found or you do not have access"
                            };

                        using (var dbConn = new SqlConnection(branch.conn))
                        {
                            dbConn.Open();

                            // confirm that the where we are getting biometrics from is at EM2001
                            var stagecode = dbConn
                                .Query<string>("select stagecode from loc_enrolProfile where formno = @formno",
                                    new { formno }).SingleOrDefault();

                            if (string.IsNullOrEmpty(stagecode) || stagecode.ToUpper() != "EM2001")
                                return new Record
                                {
                                    Error = "Biometrics record must be at EM2001"
                                };

                            // get new biometrics
                            var bioProfile = dbConn
                                .Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @formno",
                                    new {formno}).SingleOrDefault();

                            if (bioProfile == null)
                                return new Record
                                {
                                    Error = "Could not find a record matching form number entered"
                                };

                            record.FaceCurrent = bioProfile.Faceimage;
                            record.SignatureCurrent = bioProfile.Signimage;

                            return record;
                        }
                    }
                    catch (Exception ex)
                    {
                        return new Record
                        {
                            Error = "Could not find a record matching passport number entered"
                        };
                    }
                }

                return null;
            }
        }

        public Record SearchManageUser(string username, string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var user = dbConn.Query<User>("select * from [user] where loginname = @loginname", new { loginname = username }).SingleOrDefault();

                if (user == null)
                {
                    return null;
                }

                var result = new Record
                {
                    LocUser = user,
                    BranchCode = branch.BranchCode
                };

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchBranchUser,
                    Description = EnumAuditEvents.SearchBranchUser.GetEnumDescription() + $" - Login Name: {username}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });

                return result;
            }
        }

        public async Task<List<DetailedRecord>> SearchEnrolementForAnalysis(List<string> formnos, int userId)
        {
            var result = new List<DetailedRecord>();
            var appreasons = GetAppReasons();

            foreach (string form in formnos)
            {
                var formno = form;
                Record rec;
                var isPassport = char.IsLetter(formno[0]);

                // pull record from central first. Check if its a passport number or formno by first char
                if (isPassport)
                    rec = GetPassportFromCentralDocno(formno, true, userId);
                else
                    rec = await GetPassportFromCentralFormno(formno, true, userId);

                var record = new DetailedRecord();

                // If record is EM5000, use central values
                if (rec != null && rec.Passport.LocEnrolProfile.Stagecode == "EM5000")
                {
                    string branchcode = rec.Passport.LocEnrolProfile.Formno.Substring(0, 3);

                    // Set pages
                    var docpage =
                        string.IsNullOrEmpty(rec.Passport.LocEnrolProfile.Docpage) || rec.Passport.LocEnrolProfile.Docpage == "0"
                            ? "32 page"
                            : "64 page";

                    record = new DetailedRecord
                    {
                        FirstName = rec.Passport.LocDocHolderMainProfile.Firstname,
                        Surname = rec.Passport.LocDocHolderMainProfile.Surname,
                        MiddleName = rec.Passport.LocDocHolderCustomProfile.Othername1,
                        FormNo = rec.Passport.LocEnrolProfile.Formno,
                        StageCode = rec.Passport.LocEnrolProfile.Stagecode,
                        BranchCode = branchcode,
                        ApplicationId = rec.Passport.LocPaymentHistory?.Appid,
                        ReferenceId = rec.Passport.LocPaymentHistory?.Refno,
                        Appreason = rec.Passport.LocEnrolProfile.Appreason,
                        AppreasonDescr = appreasons.FirstOrDefault(x => x.Id == rec.Passport.LocEnrolProfile.Appreason)?.Name,
                        DateOfBirth = rec.Passport.LocDocHolderMainProfile.Birthdate,
                        DocPage = docpage,
                        DocType = rec.Passport.LocEnrolProfile.Doctype,
                        Remarks = rec.Passport.LocEnrolProfile.Remarks,
                        PersonalNo = rec.Passport.LocDocHolderMainProfile.Personalno,
                        Nofinger = rec.Passport.LocEnrolProfile.Nofinger == 1,
                        Title = rec.Passport.LocDocHolderCustomProfile.Title,
                        OriginState = rec.Passport.LocDocHolderCustomProfile.Originstate,
                        BirthState = rec.Passport.LocDocHolderMainProfile.Birthstate,
                        Sex = rec.Passport.LocDocHolderMainProfile.Sex,
                        EnrolTime = rec.Passport.LocEnrolProfile.Enroltime,
                        BirthTown = rec.Passport.LocDocHolderMainProfile.Birthtown,
                        FromBranch = false
                    };

                    AddBiometrics(record, rec.Passport.LocDocHolderBioProfile);
                }
                else
                {
                    var branch = GetConnectionString(formno.Substring(0, 3), userId);

                    if (branch == null)
                        continue;

                    using (var dbConn = new SqlConnection(branch.conn))
                    {
                        dbConn.Open();

                        // if its passport number, get formno
                        if (isPassport)
                        {
                            var docprofile = dbConn
                                .Query<LocDocProfile>("select * from loc_docprofile where docno = @docno and stagecode != @stagecode",
                                    new
                                    {
                                        docno = form,
                                        stagecode = EnumStageCodes.PM2001.ToString()
                                    }).SingleOrDefault();

                            if (docprofile == null)
                                return result;

                            formno = docprofile.Formno;
                        }

                        var query = @"SELECT e.*, m.*, c.*, p.*, b.*
                        FROM loc_enrolprofile e
                        LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                        LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno
                        LEFT JOIN loc_persoProfile p on e.formno = p.formno
                        LEFT JOIN loc_docHolderBioProfile b on e.formno = b.formno
                        where e.formno = @formno";

                            dbConn
                                .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile, LocPaymentHistory, LocDocHolderBioProfile,
                                    LocEnrolProfile>(
                                    query,
                                    (enroleProfile, docHolderMainpProfile, docHolderCustomProfile, paymentHistory, bioprofile) =>
                                    {
                                        record = new DetailedRecord
                                        {
                                            FirstName = docHolderMainpProfile.Firstname,
                                            Surname = docHolderMainpProfile.Surname,
                                            MiddleName = docHolderCustomProfile.Othername1,
                                            FormNo = enroleProfile.Formno,
                                            StageCode = enroleProfile.Stagecode,
                                            BranchCode = branch.BranchCode,
                                            ApplicationId = paymentHistory?.Appid,
                                            ReferenceId = paymentHistory?.Refno,
                                            Appreason = enroleProfile.Appreason,
                                            DateOfBirth = docHolderMainpProfile.Birthdate,
                                            DocPage = enroleProfile.Docpage,
                                            DocType = enroleProfile.Doctype,
                                            Remarks = enroleProfile.Remarks,
                                            PersonalNo = docHolderMainpProfile.Personalno,
                                            Nofinger = enroleProfile.Nofinger == 1,
                                            Title = docHolderCustomProfile.Title,
                                            OriginState = docHolderCustomProfile.Originstate,
                                            BirthState = docHolderMainpProfile.Birthstate,
                                            Sex = docHolderMainpProfile.Sex,
                                            EnrolTime = enroleProfile.Enroltime,
                                            BirthTown = docHolderMainpProfile.Birthtown,
                                            FromBranch = true
                                        };

                                        AddBiometrics(record, bioprofile);

                                        return enroleProfile;
                                    },
                                    new { formno },
                                    splitOn: "formno");
                    }
                }

                result.Add(record);

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() +
                                    $" - FormNo: {formno}, Branch: {(string) null}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });
            }

            return result;
        }

        public Record SearchModfyEnrolment(string formno, string branchcode, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return new Record
                {
                    Error = "Either branch not found or you do not have access"
                };

            string path = $"\\\\{branch.BranchName}\\tempFile";

            // Get file from server
            var username = ConfigurationManager.AppSettings["xmluser"];
            var pw = Helper.DecryptConfig(ConfigurationManager.AppSettings["xmlpw"], false);
            using (new NetworkConnection(path, new NetworkCredential(username, pw)))
            {
                var files = new DirectoryInfo(path).GetFiles(formno + "*.xml");
                string filename;
                if (files.Length > 1)
                {
                    var max = files.Select(x => int.Parse(x.Name[x.Name.Length - 5].ToString())).Max();
                    filename = formno + max + ".xml";
                }
                else
                    filename = files[0].Name;

                var file = File.ReadAllBytes(Path.Combine(path, filename));

                // convert to xml
                var doc = new XmlDocument();
                string xml = Encoding.UTF8.GetString(file);

                // sometimes the file has a useless char at start that causes error
                string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (xml.StartsWith(byteOrderMarkUtf8))
                {
                    xml = xml.Remove(0, byteOrderMarkUtf8.Length);
                }

                doc.LoadXml(xml);

                var record = new Record
                {
                    BranchCode = branch.BranchCode,
                    XmlFormNo = filename.Replace(".xml", ""),
                    Xml = doc.OuterXml,
                    UpdateUrl = "UpdateModifyEnrolmentFile"
                };

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchXmlRecord,
                    Description = EnumAuditEvents.SearchXmlRecord.GetEnumDescription() + $" - FormNo: {formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                return record;
            }
        }
        
        public async Task<Payment> SearchPaymentQuery(string aid, string refid, int userId)
        {
            using (var client = new HttpClient())
            {
                // Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + $"/api/payment/GetPayment?refid={refid}&aid={aid}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    // add audit
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.SearchPaymentQuery,
                        Description = EnumAuditEvents.SearchPaymentQuery.GetEnumDescription() + $" - Aid: {aid}, RefId: {refid}",
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });

                    return JsonConvert.DeserializeObject<Payment>(response.Content.ReadAsStringAsync().Result);
                }
            }

            return null;
        }

        public async Task<int> UpdatePaymentQuery(Payment payment, int userId)
        {
            using (var client = new HttpClient())
            {
                // Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + (payment.IsAdmin ? "/api/payment/EditPaymentAdmin" : "EditPayment");
                client.BaseAddress = new Uri(serviceurl);

                var response = await client.PostAsync(contracturl, new StringContent(
                    new JavaScriptSerializer().Serialize(payment), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    if (payment.Firstname?.ToUpper() != payment.OldFirstname?.ToUpper())
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Firstname from \"{payment.OldFirstname}\" to \"{payment.Firstname}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.Lastname?.ToUpper() != payment.OldSurname?.ToUpper())
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Surname from \"{payment.OldSurname}\" to \"{payment.Lastname}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.Dob != payment.OldDob)
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Date of birth from \"{payment.OldDob}\" to \"{payment.Dob}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.Expirydate != payment.OldExpiryDate)
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Expiry date from \"{payment.OldExpiryDate}\" to \"{payment.Expirydate}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.OldSex?.ToUpper() != payment.Sex?.ToUpper())
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Gender from \"{payment.OldSex}\" to \"{payment.Sex}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.OldProcessingoffice?.ToUpper() != payment.Processingoffice?.ToUpper())
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Processing office from \"{payment.OldProcessingoffice}\" to \"{payment.Processingoffice}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.OldProcessingstate?.ToUpper() != payment.Processingstate?.ToUpper())
                    {
                        AddToAudit(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Processing state from \"{payment.OldProcessingstate}\" to \"{payment.Processingstate}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    return 1;
                }
            }

            return 0;
        }

        public async Task<int> ResetPayment(int aid, int refid, int userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + $"/api/payment/ResetPayment?refid={refid}&aid={aid}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    // add audit
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.ResetPayment,
                        Description = EnumAuditEvents.ResetPayment.GetEnumDescription() + $" - Aid: {aid}, RefId: {refid}",
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
                    
                    return 1;
                }

                return 0;
            }
        }

        public int UpdateAfisRecord(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();
                
                if (enroleProfile.Stagecode == "EM2001")
                {
                    dbConn.Execute("update loc_enrolprofile set stagecode = 'EM1500', remarks = '' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateAfisRecord,
                    Description = EnumAuditEvents.UpdateAfisRecord.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });

                return 1;
            }
        }

        public Record UpdateCreateReissueRecord(Record rec, int userId)
        {
            var branch = GetConnectionString(rec.BranchCode, userId);

            // get record
            var record = SearchCreateReissueRecord(rec.DocNo, rec.BranchCode, true, userId);

            if (string.IsNullOrEmpty(record?.Passport.LocEnrolProfile.Formno))
                return null;

            // update a new record with this data
            CreateNewRecord(record, rec, branch, string.Empty);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateCreateReissueRecord,
                    Description = EnumAuditEvents.UpdateCreateReissueRecord.GetEnumDescription() + $" - OldFormNo: {rec.FormNo}, NewFormNo: {record.Passport.LocEnrolProfile.Formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo,
                    OldFormNo = rec.FormNo
                });

                return new Record
                {
                    Passport = new LocPassport
                    {
                        LocEnrolProfile = record.Passport.LocEnrolProfile,
                        LocDocHolderMainProfile = record.Passport.LocDocHolderMainProfile,
                        LocDocHolderCustomProfile = record.Passport.LocDocHolderCustomProfile,
                        LocDocHolderBioProfile = record.Passport.LocDocHolderBioProfile,
                        LocPaymentHistory = record.Passport.LocPaymentHistory
                    },
                    FaceCurrent = record.Passport.LocDocHolderBioProfile.Faceimage,
                    SignatureCurrent = record.Passport.LocDocHolderBioProfile.Signimage
                };
            }
        }

        public Record UpdateBindReissueRecord(Record rec, int userId)
        {
            var branch = GetConnectionString(rec.BranchCode, userId);

            // get record
            var record = SearchCreateReissueRecord(rec.DocNo, rec.BranchCode, true, userId);

            if (string.IsNullOrEmpty(record?.Passport.LocEnrolProfile.Formno) || string.IsNullOrEmpty(rec.FormNo))
                return null;

            // update a new record with this data
            CreateNewRecord(record, rec, branch, rec.FormNo);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateBindReissueRecord,
                    Description = EnumAuditEvents.UpdateBindReissueRecord.GetEnumDescription() + $" - OldFormNo: {rec.FormNo}, NewFormNo: {record.Passport.LocEnrolProfile.Formno}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo,
                    DocNo = rec.DocNo,
                    OldFormNo = rec.FormNo
                });

                return new Record
                {
                    Passport = new LocPassport
                    {
                        LocEnrolProfile = record.Passport.LocEnrolProfile,
                        LocDocHolderMainProfile = record.Passport.LocDocHolderMainProfile,
                        LocDocHolderCustomProfile = record.Passport.LocDocHolderCustomProfile,
                        LocDocHolderBioProfile = record.Passport.LocDocHolderBioProfile,
                        LocPaymentHistory = record.Passport.LocPaymentHistory
                    },
                    FaceCurrent = record.Passport.LocDocHolderBioProfile.Faceimage,
                    SignatureCurrent = record.Passport.LocDocHolderBioProfile.Signimage
                };
            }
        }

        public int UpdateBackToApproval(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();

               if (enroleProfile.Stagecode == "EM4000")
                {
                    dbConn.Execute("update loc_enrolprofile set stagecode = 'EM2000', Enrollocationid = @LocationId  where formno = @formno", new
                    {
                        record.FormNo,
                        record.LocationId
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateBackToApproval,
                    Description = EnumAuditEvents.UpdateBackToApproval.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateBackToPaymentCheck(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();

                if (enroleProfile.Stagecode == "EM0801" || enroleProfile.Stagecode == "EM1000")
                {
                    dbConn.Execute("update loc_enrolprofile set stagecode = 'EM0800' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateBackToPaymentCheck,
                    Description = EnumAuditEvents.UpdateBackToPaymentCheck.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateBackToPerso(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where formno = @formno", new { record.FormNo }).Single();
                var docprofile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { record.FormNo }).First();

                if (persoProfile.Stagecode == "PM2000")
                {
                    dbConn.Execute("update loc_persoprofile set stagecode = 'PM2001', Persolocationid = @PersoLocationId where formno = @formno", new
                    {
                        record.FormNo,
                        record.PersoLocationId
                    });
                }

                if (docprofile.Stagecode == "PM2000")
                {
                    dbConn.Execute("update loc_docprofile set stagecode = 'PM2001' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateBackToPaymentCheck,
                    Description = EnumAuditEvents.UpdateBackToPaymentCheck.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateFixWrongPassportIssued(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var docProfiles = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { record.FormNo }).ToList();

                // There must be 2 docprofiles
                if (docProfiles.Count != 2)
                {
                    return 3;
                }

                string booklet1 = "";
                string booklet2 = "";

                // change the stage code
                foreach (var profile in docProfiles)
                {
                    // check if this passport has a perso profile
                    var perso = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where docno = @docno", new { profile.Docno }).SingleOrDefault();

                    if (perso == null)
                    {
                        dbConn.Execute("update loc_docprofile set stagecode = 'PM2001' where formno = @formno", new
                        {
                            profile.Formno
                        });

                        booklet2 = profile.Docno;
                    }
                    else
                    {
                        dbConn.Execute("update loc_docprofile set stagecode = 'EM4200' where formno = @formno", new
                        {
                            profile.Formno
                        });

                        booklet1 = profile.Docno;
                    }
                }

                // update enrolprofile
                dbConn.Execute("update loc_enrolprofile set stagecode = 'EM4200' where formno = @formno", new
                {
                    record.FormNo
                });

                // add audit for enrol change
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateFixWrongPassportIssued,
                    Description = EnumAuditEvents.UpdateFixWrongPassportIssued.GetEnumDescription() + $" - Booklet1: {booklet1} to EM4200, Booklet2: {booklet2} to PM2001. Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });

                return 1;
            }
        }

        public int UpdateResendJobForPrinting(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where formno = @formno", new { record.FormNo }).Single();
                var docProfile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { record.FormNo }).First();

                // change the stage code
                if (docProfile.Stagecode == "PM1000")
                {
                    dbConn.Execute("update loc_docprofile set stagecode = 'PM0500' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                if (persoProfile.Stagecode == "PM1000")
                {
                    dbConn.Execute("update loc_persoprofile set stagecode = 'PM0500' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                // add audit for enrol change
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateResendJobForPrinting,
                    Description = EnumAuditEvents.UpdateResendJobForPrinting.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateIssuePassport(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();
                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where formno = @formno", new { record.FormNo }).Single();
                var docProfile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { record.FormNo }).First();

                // change the stage code
                if (persoProfile.Stagecode == "PM2000")
                {
                    if (enroleProfile.Stagecode == "EM4000")
                    {
                        dbConn.Execute("update loc_enrolprofile set stagecode = 'EM4200', issuetime = @issuetime, issueby = 'Iris Support' where formno = @formno", new
                        {
                            record.FormNo,
                            issuetime = DateTime.Now,
                        });
                    }

                    if (docProfile.Stagecode == "PM2000")
                    {
                        dbConn.Execute("update loc_docprofile set stagecode = 'EM4200', issuetime = @issuetime, issueby = 'Iris Support', issueplace = @issueplace where formno = @formno", new
                        {
                            record.FormNo,
                            issuetime = DateTime.Now,
                            issueplace = branch.BranchName
                        });
                    }
                }

                // add audit for enrol change
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateResendJobForPrinting,
                    Description = EnumAuditEvents.UpdateResendJobForPrinting.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateSendToIssuance(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where formno = @formno", new { record.FormNo }).Single();
                var docProfile = dbConn.Query<LocDocProfile>("select * from loc_docprofile where formno = @formno", new { record.FormNo }).First();

                // change the stage code
                if (persoProfile.Stagecode == "PM2001" || persoProfile.Stagecode == "PM1000")
                {
                    dbConn.Execute("update loc_persoprofile set stagecode = 'PM2000' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                if (docProfile.Stagecode == "PM2001" || docProfile.Stagecode == "PM1000")
                {
                    dbConn.Execute("update loc_docprofile set stagecode = 'PM2000' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateSendToIssuance,
                    Description = EnumAuditEvents.UpdateSendToIssuance.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateEnrolmentRecord(DetailedRecord record, int userId)
        {
            if (!(HttpContext.Current.Session["TempRecord"] is DetailedRecord oldrecord))
                return 0;

            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                // update records
                if (record.FirstName?.ToUpper() != oldrecord.FirstName?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.FirstName) && string.IsNullOrEmpty(record.FirstName)))
                {
                    var prev = oldrecord.FirstName;

                    if (record.FirstName == null)
                        record.FirstName = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set firstname = @firstname where formno = @formno", new { firstname = record.FirstName.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Firstname from \"{prev}\" to \"{record.FirstName.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.Surname.ToUpper() != oldrecord.Surname?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.Surname) && string.IsNullOrEmpty(record.Surname)))
                {
                    var prev = oldrecord.Surname;

                    if (record.Surname == null)
                        record.Surname = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set surname = @surname where formno = @formno", new { surname = record.Surname.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Surname from \"{prev}\" to \"{record.Surname.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.DateOfBirth != oldrecord.DateOfBirth)
                {
                    var prev = oldrecord.DateOfBirth.Value.ToShortDateString();

                    dbConn.Execute("update loc_docHolderMainProfile set birthdate = @birthdate where formno = @formno", new { birthdate = record.DateOfBirth, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Date of birth from \"{prev}\" to \"{record.DateOfBirth.Value.ToShortDateString()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.Sex != oldrecord.Sex && !(string.IsNullOrEmpty(oldrecord.Sex) && string.IsNullOrEmpty(record.Sex)))
                {
                    var prev = oldrecord.Sex;

                    if (record.Sex == null)
                        record.Sex = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set sex = @sex where formno = @formno", new { sex = record.Sex.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Gender from \"{prev}\" to \"{record.Sex}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.BirthTown.ToUpper() != oldrecord.BirthTown?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.BirthTown) && string.IsNullOrEmpty(record.BirthTown)))
                {
                    var prev = oldrecord.BirthTown;

                    if (record.BirthTown == null)
                        record.BirthTown = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set BirthTown = @BirthTown where formno = @formno", new { BirthTown = record.BirthTown.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Birth Town from \"{prev}\" to \"{record.BirthTown.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.BirthState.ToUpper() != oldrecord.BirthState?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.BirthState) && string.IsNullOrEmpty(record.BirthState)))
                {
                    var prev = oldrecord.BirthState;

                    if (record.BirthState == null)
                        record.BirthState = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set BirthState = @BirthState where formno = @formno", new { BirthState = record.BirthState.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Birth state from \"{prev}\" to \"{record.BirthState.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }


                if (record.Title?.ToUpper() != oldrecord.Title?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.Title) && string.IsNullOrEmpty(record.Title)))
                {
                    var prev = oldrecord.Title;

                    if (record.Title == null)
                        record.Title = string.Empty;

                    dbConn.Execute("update loc_docHolderCustomProfile set Title = @Title where formno = @formno", new { Title = record.Title.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Title from \"{prev}\" to \"{record.Title}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.MiddleName?.ToUpper() != oldrecord.MiddleName?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.MiddleName) && string.IsNullOrEmpty(record.MiddleName)))
                {
                    var prev = oldrecord.MiddleName;
                    if (record.MiddleName == null)
                        record.MiddleName = string.Empty;

                    dbConn.Execute("update loc_docHolderCustomProfile set OtherName1 = @MiddleName where formno = @formno", new { MiddleName = record.MiddleName.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Middlename from \"{prev}\" to \"{record.MiddleName.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.OriginState?.ToUpper() != oldrecord.OriginState?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.OriginState) && string.IsNullOrEmpty(record.OriginState)))
                {
                    var prev = oldrecord.OriginState;

                    if (record.OriginState == null)
                        record.OriginState = string.Empty;

                    dbConn.Execute("update loc_docHolderCustomProfile set OriginState = @OriginState where formno = @formno", new { OriginState = record.OriginState.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Origin state from \"{prev}\" to \"{record.OriginState.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.Appreason != oldrecord.Appreason)
                {
                    var prev = oldrecord.Appreason;

                    dbConn.Execute("update loc_enrolProfile set Appreason = @Appreason where formno = @formno", new {record.Appreason, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Appreason from \"{prev}\" to \"{record.Appreason}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.DocPage != oldrecord.DocPage && !(string.IsNullOrEmpty(oldrecord.DocPage) && string.IsNullOrEmpty(record.DocPage)))
                {
                    var prev = oldrecord.DocPage;

                    dbConn.Execute("update loc_enrolProfile set DocPage = @DocPage where formno = @formno", new {record.DocPage, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed DocPage from \"{prev}\" to \"{record.DocPage}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.DocType != oldrecord.DocType && !(string.IsNullOrEmpty(oldrecord.DocType) && string.IsNullOrEmpty(record.DocType)))
                {
                    var prev = oldrecord.DocType;

                    dbConn.Execute("update loc_enrolProfile set DocType = @DocType where formno = @formno", new {record.DocType, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed DocType from \"{prev}\" to \"{record.DocType}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.PersonalNo?.ToUpper() != oldrecord.PersonalNo?.ToUpper() && !(string.IsNullOrEmpty(oldrecord.PersonalNo) && string.IsNullOrEmpty(record.PersonalNo)))
                {
                    var prev = oldrecord.PersonalNo;

                    if (record.PersonalNo == null)
                        record.PersonalNo = string.Empty;

                    dbConn.Execute("update loc_docHolderMainProfile set PersonalNo = @PersonalNo where formno = @formno", new { PersonalNo = record.PersonalNo.ToUpper(), record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed PersonalNo from \"{prev}\" to \"{record.PersonalNo.ToUpper()}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.Remarks != oldrecord.Remarks?.Trim() && !(string.IsNullOrEmpty(oldrecord.Remarks) && string.IsNullOrEmpty(record.Remarks)))
                {
                    if (record.Remarks == null)
                        record.Remarks = string.Empty;

                    dbConn.Execute("update loc_enrolProfile set Remarks = @Remarks where formno = @formno", new { Remarks = record.Remarks.ToUpper(), record.FormNo });

                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Updated remark, Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.Nofinger != oldrecord.Nofinger)
                {
                    var prev = oldrecord.Nofinger;

                    dbConn.Execute("update loc_enrolProfile set Nofinger = @Nofinger where formno = @formno", new {record.Nofinger, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Nofinger from \"{prev}\" to \"{record.Nofinger}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.ApplicationId != oldrecord.ApplicationId)
                {
                    var prev = oldrecord.ApplicationId;

                    dbConn.Execute("update loc_paymentHistory set appid = @ApplicationId where formno = @formno", new { record.ApplicationId, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed ApplicationId from \"{prev}\" to \"{record.ApplicationId}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.EnrolLocationId != oldrecord.EnrolLocationId)
                {
                    var prev = oldrecord.EnrolLocationId;

                    dbConn.Execute("update loc_enrolProfile set enrollocationid = @enrollocationid where formno = @formno", new { record.EnrolLocationId, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Enrollocationid from \"{prev}\" to \"{record.EnrolLocationId}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }

                if (record.ReferenceId != oldrecord.ReferenceId)
                {
                    var prev = oldrecord.ReferenceId;

                    dbConn.Execute("update loc_paymentHistory set refno = @ReferenceId where formno = @formno", new {record.ReferenceId, record.FormNo });
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                        Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed ReferenceId from \"{prev}\" to \"{record.ReferenceId}\", Branch: {branch.BranchName}, FormNo: {record.FormNo}",
                        TimeStamp = DateTime.Now,
                        UserId = userId,
                        FormNo = record.FormNo
                    });
                }
            }

            return 1;
        }

        public int UpdateOverideBookletSequence(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var stagecode = dbConn.Query<string>("select stagecode from loc_docinventory where docno = @docno", new { docno = record.PassportNo }).First();

                if (stagecode == "IM2000")
                    dbConn.Execute("update loc_docinventory set stagecode = 'IM3000' where docno = @docno", new
                    {
                        docno = record.PassportNo
                    });

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateOverideBookletSequence,
                    Description = EnumAuditEvents.UpdateOverideBookletSequence.GetEnumDescription() + $" - DocNo: {record.PassportNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = record.DocNo
                });
            }

            return 1;
        }

        public int UpdateEditBookletChipNo(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                dbConn.Execute("update loc_docinventory set chipsn = @chipsn where docno = @docno", new
                {
                    record.ChipSn,
                    docno = record.PassportNo
                });

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateEditBookletChipNo,
                    Description = EnumAuditEvents.UpdateEditBookletChipNo.GetEnumDescription() + $" - DocNo: {record.PassportNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = record.DocNo
                });
            }

            return 1;
        }

        public int UpdateBookletToPersoQueue(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var docinventory = dbConn.Query<LocDocProfile>("select * from loc_docinventory where docno = @docno", new { docno = record.PassportNo }).First();

                if (docinventory.Stagecode == "IM3000")
                    dbConn.Execute("update loc_docinventory set stagecode = 'IM2000' where docno = @docno", new
                    {
                        docno = record.PassportNo
                    });

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateOverideBookletSequence,
                    Description = EnumAuditEvents.UpdateOverideBookletSequence.GetEnumDescription() + $" - DocNo: {record.PassportNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = record.DocNo
                });
            }

            return 1;
        }

        public int UpdateEditBioRecord(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var bioProfile1 = dbConn.Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @formno", new { record.FormNo }).Single();
                var bioProfile2 = dbConn.Query<LocDocHolderBioProfile>("select * from loc_docHolderBioProfile where formno = @secondformno", new { record.SecondFormNo }).Single();

                using (var central = new SqlConnection(_auth))
                {
                    central.Open();
                    // save the current one
                    central.Execute(
                        "insert into EditBioRecordStorage (formno, UpdateWithFormNo, face, facej2k, signature, datechanged, userid) " +
                        "values (@formno, @UpdateWithFormNo, @face, @facej2k, @signature, @datechanged, @userid)", new
                        {
                            record.FormNo,
                            UpdateWithFormNo = record.SecondFormNo,
                            Face = bioProfile1.Faceimage,
                            facej2k = bioProfile1.Faceimagej2K,
                            Signature = bioProfile1.Signimage,
                            DateChanged = DateTime.Now,
                            UserId = userId
                        });
                }

                // overwrite the images
                if (record.ReplaceType == 1)
                    dbConn.Execute("update loc_docHolderBioProfile set signimage = @Signimage where formno = @formno", new
                    {
                        record.FormNo,
                        bioProfile2.Signimage
                    });

                if (record.ReplaceType == 2)
                    dbConn.Execute("update loc_docHolderBioProfile set faceimage = @Faceimage, faceimagej2k = @faceimagej2k where formno = @formno", new
                    {
                        record.FormNo,
                        bioProfile2.Faceimage,
                        bioProfile2.Faceimagej2K
                    });

                if (record.ReplaceType == 3)
                {
                    dbConn.Execute("update loc_docHolderBioProfile set faceimage = @Faceimage, faceimagej2k = @faceimagej2k, signimage = @Signimage where formno = @formno", new
                    {
                        record.FormNo,
                        bioProfile2.Faceimage,
                        bioProfile2.Faceimagej2K,
                        bioProfile2.Signimage
                    });
                }

                // add audit for enrol change
                string type = record.ReplaceType == 1 ? "Signature" : record.ReplaceType == 2 ? "Face" : "Both";
                
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateEditBioRecord,
                    Description = EnumAuditEvents.UpdateEditBioRecord.GetEnumDescription() + $" - FormNoOld: {record.FormNo}, FormNoNew: {record.SecondFormNo}, " +
                                  $"Type: {type}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            return 1;
        }

        public int UpdateManageUser(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var user = dbConn.Query<User>("select * from [user] where loginname = @loginname", new { loginname = record.Username.ToLower() }).Single();

                string disabled = string.Empty;
                bool isdisabled = record.Disabled == 1;
                if (user.Disabled != isdisabled)
                    disabled = "Disabled: " + isdisabled;

                string extendby = string.Empty;
                if (record.ExtendBy != 0)
                    extendby = record.ExtendBy + " months";

                user.Disabled = isdisabled;
                user.PasswordExpires = record.GetNewUserPasswordExpiry(user.PasswordExpires, record.ExtendBy);

                // For some reason, HQ has different table structure. Freakin Iris Malaysia ~_~
                if (branch.BranchCode == "369")
                {
                    var newdate = DateTime.ParseExact(user.PasswordExpires.ToString(), "yyyyMMddHHmmssfff", CultureInfo.CurrentCulture);
                    dbConn.Execute("update [user] set disabled = @disabled, passwordexpires = @passwordexpires, passwordexpirydate = @passwordexpirydate where loginname = @loginname", new
                    {
                        loginname = record.Username.ToLower(),
                        user.Disabled,
                        passwordexpirydate = newdate.ToString("yyyy-MM-dd HH:mm:ss:fff"),
                        user.PasswordExpires
                    });
                }
                else
                {
                    dbConn.Execute("update [user] set disabled = @disabled, passwordexpires = @passwordexpires where loginname = @loginname", new
                    {
                        loginname = record.Username.ToLower(),
                        user.Disabled,
                        user.PasswordExpires
                    });
                }

                // add audit for enrol change
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateBranchUser,
                    Description = EnumAuditEvents.UpdateBranchUser.GetEnumDescription() + $" - Branch: {branch.BranchName}, {disabled} {extendby}",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            return 1;
        }

        public int UpdateReverseEditBioRecord(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                EditBioRecordStore previous;
                using (var central = new SqlConnection(_auth))
                {
                    central.Open();

                    // get the previous one
                    previous = central
                        .Query<EditBioRecordStore>(
                            "select * from EditBioRecordStorage where formno = @formno order by datechanged desc",
                            new { record.FormNo }).FirstOrDefault();

                    if (previous == null)
                        return 0;
                }

                // overwrite image with old
                dbConn.Execute("update loc_docHolderBioProfile set faceimage = @faceimage, faceimagej2k = @faceimagej2k, signimage = @signimage where formno = @formno", new
                {
                    faceimage = previous.Face,
                    faceimagej2k = previous.FaceJ2k,
                    signimage = previous.Signature,
                    record.FormNo
                });
               
                // add audit for bio revert
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateEditBioRecordRevert,
                    Description = EnumAuditEvents.UpdateEditBioRecordRevert.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });

                // Delete it from storage now that it has been restored
                using (var central = new SqlConnection(_auth))
                {
                    central.Open();

                    central.Execute("delete from EditBioRecordStorage where id = @id", new { previous.Id });
                }
               
            }

            return 1;
        }

        public int UpdateModifyEnrolmentFile(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            // save xml back to server
            string path = $"\\\\{branch.BranchName}\\tempFile";
            var username = ConfigurationManager.AppSettings["xmluser"];
            var pw = Helper.DecryptConfig(ConfigurationManager.AppSettings["xmlpw"], false);

            using (new NetworkConnection(path, new NetworkCredential(username, pw)))
            {
                string backupFolder = Path.Combine(path, "Original Records");
                string current = Path.Combine(path, record.XmlFormNo + ".xml");
                string backup = Path.Combine(backupFolder, record.XmlFormNo + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");

                // create folder of edited records if it doesn't exist
                Directory.CreateDirectory(Path.Combine(path, "Original Records"));

                // Save backup of current file
                File.Copy(current, backup);

                // Overwrite current
                var xml = new XmlDocument();

                // remove space:"preserve" tag
                var doc = XDocument.Parse(record.Xml);

                bool PreserveAttrFunc(XAttribute atr) => atr.Name.LocalName == "space" && atr.Value == "preserve";
                var descendants = doc.Descendants().Where(kp => kp.HasAttributes && kp.Attributes().Any(PreserveAttrFunc));

                foreach (var descendant in descendants)
                {
                    descendant.Attributes().FirstOrDefault(PreserveAttrFunc)?.Remove();
                }

                doc.AddFirst(xml.CreateXmlDeclaration("1.0", "UTF-8", null));
                doc.Save(current);
            }

            // add audit for change
            AddToAudit(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateXmlFile,
                Description = EnumAuditEvents.UpdateXmlFile.GetEnumDescription() + $" - Branch: {branch.BranchName}, XML: {record.XmlFormNo}.xml",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            return 1;
        }

        public int UpdateInventoryManifest(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            var inventoryserver = ConfigurationManager.AppSettings["inventoryserver"];

            // Get XML
            string path = $"\\\\{inventoryserver}";
            bool savebatch = true;
            var booklets = new List<Booklet>();

            // Get file from server
            var xmlpath = new DirectoryInfo(path).GetFiles($"[{record.BranchCode}][{record.SelectedXml}]*.xml").First();
            var xdoc = XDocument.Load(xmlpath.FullName);

            // Get outer values in file
            var branchid = xdoc.Root.Element("FileInfo")?.Elements("Info").First().Attribute("branchId")?.Value;
            var boxsn = xdoc.Root.Element("PPInfo")?.Elements("box").First().Attribute("id")?.Value;
            var entrytime = DateTime.Now;
            var doctype = xdoc.Root.Element("PPInfo")?.Elements("box").First().Attribute("PPType")?.Value;
                
            // if this has been run before, check that they were completed
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();

                var prevbooklets = dbConn.Query<Booklet>("select * from inventoryqueue where boxsn = @boxsn", new { boxsn }).ToList();

                // Don't save any. Just upload the ones left
                if (prevbooklets.Count != 0)
                {
                    savebatch = false;
                    booklets = prevbooklets;
                }
            }

            if (savebatch)
            {
                // Save all the booklets 
                using (var dbConn = new SqlConnection(_auth))
                {
                    dbConn.Open();

                    foreach (var entry in xdoc.Root.Element("PPInfo").Elements("box").First().Elements("pp"))
                    {

                        var docno = entry.Attribute("sn")?.Value;
                        var chipsn = entry.Attribute("chipsn")?.Value;
                        var docpage = entry.Attribute("docpage")?.Value;

                        // add to list
                        booklets.Add(new Booklet
                        {
                            BoxSn = boxsn,
                            EntryTime = entrytime,
                            BranchCode = branchid,
                            DateCreated = entrytime,
                            DocNo = docno,
                            DocPage = docpage,
                            DocType = doctype,
                            ChipSn = chipsn,
                            StageCode = "IM2000"
                        });

                        dbConn.Execute(
                            "insert into inventoryQueue (chipsn, doctype, docno, branchcode, stagecode, boxsn, entrytime, docpage) values " +
                            "(@chipsn, @doctype, @docno, @branchcode, @stagecode, @boxsn, @entrytime, @docpage)",
                            new
                            {
                                chipsn,
                                doctype,
                                docno,
                                branchcode = branchid,
                                stagecode = "IM2000",
                                boxsn,
                                entrytime,
                                docpage
                            });
                    }
                }
            }

            // Now run all for this branch
            var counter = 1;
            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                try
                {
                    foreach (var booklet in booklets)
                    {
                        dbConn.Execute(
                            "insert into loc_docinventory (chipsn, doctype, docno, branchcode, stagecode, boxsn, entrytime, docpage) values " +
                            "(@chipsn, @doctype, @docno, @branchcode, @stagecode, @boxsn, @entrytime, @docpage)",
                            new
                            {
                                booklet.ChipSn,
                                booklet.DocType,
                                booklet.DocNo,
                                booklet.BranchCode,
                                booklet.StageCode,
                                booklet.BoxSn,
                                booklet.EntryTime,
                                booklet.DocPage
                            });

                        booklet.Sent = 1;
                        counter++;
                    }
                }
                catch (Exception ex)
                {
                    if (counter == 1 && ex.Message.Contains("Cannot insert duplicate key"))
                    {
                        // basically this one has been done before
                        using (var authcon = new SqlConnection(_auth))
                        {
                            authcon.Open();

                            authcon.Execute(
                                $"delete from inventoryQueue where chipsn in ({string.Join(",", booklets.Select(x => "'" + x.ChipSn + "'").ToList())})");
                        }

                        return 2;
                    }

                    new AuthService().SaveError(ex, "UploadInventory", userId.ToString());

                    // delete those that were sent
                    using (var authcon = new SqlConnection(_auth))
                    {
                        authcon.Open();

                        var sentlist = booklets.Where(x => x.Sent == 1).Select(b => "'" + b.ChipSn + "'").ToList();
                        authcon.Execute(
                            $"delete from inventoryQueue where chipsn in ({string.Join(",", sentlist)})");
                    }

                    return 3;
                }
                    
            }

            // update queue
            using (var authcon = new SqlConnection(_auth))
            {
                authcon.Open();

                authcon.Execute(
                    $"delete from inventoryQueue where id in ({string.Join(",", booklets.Select(x => x.Id).ToList())})");
            }

            // add audit for upload
            AddToAudit(new Audit
            {
                EventId = (int)EnumAuditEvents.UploadXmlToBrachInventory,
                Description = EnumAuditEvents.UploadXmlToBrachInventory.GetEnumDescription() +
                                $" - Branch: {branch.BranchName}, Range: {record.SelectedXml}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            return 1;
        }

        public int UpdateManuallyFail(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();

                if (enroleProfile.Stagecode == "EM2000" || enroleProfile.Stagecode == "EM4000" || enroleProfile.Stagecode.StartsWith("G"))
                {
                    dbConn.Execute("update loc_enrolprofile set stagecode = 'EM2001', Remarks = @remark  where formno = @formno", new
                    {
                        record.FormNo,
                        record.Remark
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateManuallyFail,
                    Description = EnumAuditEvents.UpdateManuallyFail.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchName}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });

            }

            return 1;
        }
        
        public int UpdateResetStagecode(Record record, int userId)
        {
            var branch = GetConnectionString(record.BranchCode, userId);

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var enroleProfile = dbConn.Query<LocEnrolProfile>("select * from loc_enrolprofile where formno = @formno", new { record.FormNo }).Single();

                if (enroleProfile.Stagecode == "EM0801")
                {
                    dbConn.Execute("update loc_enrolprofile set stagecode = 'EM1000' where formno = @formno", new
                    {
                        record.FormNo
                    });
                }

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateResetStagecode,
                    Description = EnumAuditEvents.UpdateResetStagecode.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {branch.BranchCode}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });

            }

            return 1;
        }

        public List<LocalQuery> SearchLocalQuery(string docno, string branchcode, string gender, string firstname, string surname, string formno, int userId)
        {
             var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return null;

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                string where = string.Empty;
                var audit = new List<string>
                {
                    "Branch: " + branch.BranchName
                };

                if (!string.IsNullOrEmpty(formno))
                {
                    audit.Add("FormNo: " + formno);
                    where = "where e.formno = @formno ";
                }

                if (!string.IsNullOrEmpty(docno))
                {
                    audit.Add("DocNo: " + docno);
                    where = where == "" ? "where d.docno = @docno" : " and d.docno = @docno";
                }

                if (!string.IsNullOrEmpty(firstname))
                {
                    audit.Add("Firstname: " + firstname);
                    where = where == "" ? "where m.firstname = @firstname" : " and m.firstname = @firstname";
                }


                if (!string.IsNullOrEmpty(surname))
                {
                    audit.Add("Surname: " + surname);
                    where = where == "" ? "where m.surname = @surname" : " and m.surname = @surname";
                }

                if (!string.IsNullOrEmpty(gender))
                {
                    audit.Add("Gender: " + gender);
                    where = where == "" ? "where m.sex = @sex" : " and m.sex = @sex";
                }

                var query = @"SELECT e.*, m.*, c.*, p.*, b.*, d.*
                    FROM loc_enrolprofile e
                    LEFT JOIN loc_docHolderMainProfile m on e.formno = m.formno
                    LEFT JOIN loc_docHolderCustomProfile c on e.formno = c.formno                    
                    LEFT JOIN loc_persoProfile p on e.formno = p.formno
                    LEFT JOIN loc_docHolderBioProfile b on e.formno = b.formno
                    LEFT JOIN loc_docProfile d on e.formno = d.formno " + where;

                var result = new List<LocalQuery>();
                dbConn
                    .Query<LocEnrolProfile, LocDocHolderMainProfile, LocDocHolderCustomProfile, LocPersoProfile, LocDocHolderBioProfile, LocDocProfile, LocEnrolProfile>(
                        query,
                        (enroleProfile, docHolderMainpProfile, docHolderCustomProfile, persoProfile, bioProfile, docProfile) =>
                        {
                            result.Add(new LocalQuery
                            {
                                EnrolProfile = enroleProfile,
                                DocHolderMainProfile = docHolderMainpProfile,
                                DocHolderCustomProfile = docHolderCustomProfile,
                                PersoProfile = persoProfile,
                                BioProfile = bioProfile,
                                DocProfile = docProfile,
                            });

                            return enroleProfile;
                        },
                        new
                        {
                            formno,
                            docno = docno?.ToUpper(),
                            firstname = firstname?.ToUpper(),
                            surname = surname?.ToUpper(),
                            sex = gender?.ToUpper()
                        },
                        splitOn: "formno, docno");

                // add audit for enrol change
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.LocalQuery,
                    Description = EnumAuditEvents.LocalQuery.GetEnumDescription() + " where " + string.Join(", ", audit),
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                // order by perso docno
                if (result.Count != 0)
                {
                    var persodocno = result.FirstOrDefault(x => x.PersoProfile != null)?.PersoProfile.Docno;
                    if (persodocno != null)
                        result = result.OrderByDescending(x => x.DocProfile.Docno == persodocno).ToList();
                }
               
                return result;
            }
        }
        
        private void CreateNewRecord(Record record, Record rec, Branch branch, string bioprofileFormNo)
        {
            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                // Create new tables
                var formno = dbConn.Query<int>("select min(FormNo) from FormCache").First();

                var newformno = branch.BranchCode + formno.ToString("000000000");

                // Delete the formno now that it has been taken
                dbConn.Execute("delete from formcache where formno = @formno", new
                {
                    formno
                });

                // If no more forms, create some new ones
                var count = dbConn.Query<int>("select count(FormNo) from FormCache").First();

                if (count == 0)
                {
                    var newforms = Enumerable.Range(formno + 1, 20).ToList();
                    foreach (int newform in newforms)
                    {
                        dbConn.Execute("insert into formcache (formno) values (@newform)", new {newform});
                    }
                }

                // get intergrated branches
                var integrated = ConfigurationManager.AppSettings["integratedBranches"].Split(',').ToList();
                
                record.Passport.LocEnrolProfile.Formno = newformno;
                record.Passport.LocEnrolProfile.Appby = string.Empty;
                record.Passport.LocEnrolProfile.Apptime = null;
                record.Passport.LocEnrolProfile.Overwriteadminrejectby = string.Empty;
                record.Passport.LocEnrolProfile.Overwriteadminrejecttime = null;
                record.Passport.LocEnrolProfile.Overwritepriorityby = string.Empty;
                record.Passport.LocEnrolProfile.Overwriteprioritytime = null;
                record.Passport.LocEnrolProfile.Overwriteafisby = string.Empty;
                record.Passport.LocEnrolProfile.Overwriteafistime = null;
                record.Passport.LocEnrolProfile.Overwriteotherby = string.Empty;
                record.Passport.LocEnrolProfile.Overwriteothertime = null;
                record.Passport.LocEnrolProfile.Thirdpartyissueby = string.Empty;
                record.Passport.LocEnrolProfile.Thirdpartyissuetime = null;
                record.Passport.LocEnrolProfile.Issueby = string.Empty;
                record.Passport.LocEnrolProfile.Issuetime = null;
                record.Passport.LocEnrolProfile.Appreason = rec.AppReason;
                record.Passport.LocEnrolProfile.Olddocno = rec.DocNo;
                record.Passport.LocEnrolProfile.Branchcode = branch.BranchCode;
                record.Passport.LocEnrolProfile.Enrollocationid = 9115001;
                record.Passport.LocEnrolProfile.Nofinger = string.IsNullOrEmpty(bioprofileFormNo) ? 1 : 0;
                record.Passport.LocEnrolProfile.Stagecode =
                    rec.DocNo.ToUpper()[0].ToString() == "D" || !integrated.Contains(branch.BranchCode) ? "EM1000" : "EM0800";

                record.Passport.LocPaymentHistory.Appid = rec.AppId;
                record.Passport.LocPaymentHistory.Refno = rec.RefId;

                try
                {
                    // integrated branch
                    dbConn.Execute(
                        @"insert into loc_enrolprofile (formno, appreason, branchcode, enrollocationid, layoutid, olddocno, doctype, fileno, nofinger, remarks, stagecode, priority, docpage, enrolby, enroltime) 
                            values (@formno, @appreason, @branchcode, @enrollocationid, @layoutid, @olddocno, @doctype, @fileno, @nofinger, @remarks, @stagecode, @priority, @docpages, @enrolby, @enroltime)",
                        new
                        {
                            record.Passport.LocEnrolProfile.Formno,
                            record.Passport.LocEnrolProfile.Appreason,
                            record.Passport.LocEnrolProfile.Olddocno,
                            record.Passport.LocEnrolProfile.Stagecode,
                            record.Passport.LocEnrolProfile.Branchcode,
                            record.Passport.LocEnrolProfile.Enrollocationid,
                            record.Passport.LocEnrolProfile.Layoutid,
                            record.Passport.LocEnrolProfile.Doctype,
                            record.Passport.LocEnrolProfile.Fileno,
                            record.Passport.LocEnrolProfile.Nofinger,
                            record.Passport.LocEnrolProfile.Remarks,
                            record.Passport.LocEnrolProfile.Priority,
                            rec.DocPages,
                            rec.EnrolBy,
                            enroltime = DateTime.Now
                        });
                }
                catch (Exception ex)
                {
                    dbConn.Execute(
                        @"insert into loc_enrolprofile (formno, appreason, branchcode, enrollocationid, layoutid, olddocno, doctype, fileno, nofinger, remarks, stagecode, priority, enrolby, enroltime) 
                            values (@formno, @appreason, @branchcode, @enrollocationid, @layoutid, @olddocno, @doctype, @fileno, @nofinger, @remarks, @stagecode, @priority, @enrolby, @enroltime)",
                        new
                        {
                            record.Passport.LocEnrolProfile.Formno,
                            record.Passport.LocEnrolProfile.Appreason,
                            record.Passport.LocEnrolProfile.Olddocno,
                            record.Passport.LocEnrolProfile.Stagecode,
                            record.Passport.LocEnrolProfile.Branchcode,
                            record.Passport.LocEnrolProfile.Enrollocationid,
                            record.Passport.LocEnrolProfile.Layoutid,
                            record.Passport.LocEnrolProfile.Doctype,
                            record.Passport.LocEnrolProfile.Fileno,
                            record.Passport.LocEnrolProfile.Nofinger,
                            record.Passport.LocEnrolProfile.Remarks,
                            record.Passport.LocEnrolProfile.Priority,
                            rec.EnrolBy,
                            enroltime = DateTime.Now
                        });
                }

                // DocHolderMainProfile
                record.Passport.LocDocHolderMainProfile.Formno = newformno;
                dbConn.Execute(
                    @"insert into loc_docholdermainprofile (formno, surname, firstname, birthdate, birthtown, birthstate, sex, nationality, personalno) 
                            values (@formno, @surname, @firstname, @birthdate, @birthtown, @birthstate, @sex, @nationality, @personalno)",
                    new
                    {
                        record.Passport.LocDocHolderMainProfile.Formno,
                        record.Passport.LocDocHolderMainProfile.Firstname,
                        record.Passport.LocDocHolderMainProfile.Surname,
                        record.Passport.LocDocHolderMainProfile.Birthdate,
                        record.Passport.LocDocHolderMainProfile.Birthstate,
                        record.Passport.LocDocHolderMainProfile.Birthtown,
                        record.Passport.LocDocHolderMainProfile.Sex,
                        record.Passport.LocDocHolderMainProfile.Nationality,
                        record.Passport.LocDocHolderMainProfile.Personalno
                    });

                // DocHolderCustomProfile
                record.Passport.LocDocHolderCustomProfile.Formno = newformno;
                dbConn.Execute(
                    "insert into loc_docholdercustomprofile (formno, othername1, othername2, othername3, othername4, dobflag, originstate, addstreet, addtown, addstate, " +
                    "profession, occupation, height, eyecolor, haircolor, maritalstatus, birthmark, maidenname, nokname, nokaddress, child1sname, child1fname, child1dob, " +
                    "child1pobt, child1pobs, child1sex, child2sname, child2fname, child2dob, child2pobt, child2pobs, child2sex, child3sname, child3fname, child3dob, child3pobt, " +
                    "child3pobs, child3sex, child4sname, child4fname, child4dob, child4pobt, child4pobs, child4sex, title, eyecolorother, haircolorother, addpermanent, email, contactphone, " +
                    "mobilephone, gtoname, gtoaddress) values (@formno, @othername1, @othername2, @othername3, @othername4, @dobflag, @originstate, @addstreet, @addtown, @addstate, @" +
                    "profession, @occupation, @height, @eyecolor, @haircolor, @maritalstatus, @birthmark, @maidenname, @nokname, @nokaddress, @child1sname, @child1fname, @child1dob, @" +
                    "child1pobt, @child1pobs, @child1sex, @child2sname, @child2fname, @child2dob, @child2pobt, @child2pobs, @child2sex, @child3sname, @child3fname, @child3dob, @child3pobt, @" +
                    "child3pobs, @child3sex, @child4sname, @child4fname, @child4dob, @child4pobt, @child4pobs, @child4sex, @title, @eyecolorother, @haircolorother, @addpermanent, @email, @contactphone, @" +
                    "mobilephone, @gtoname, @gtoaddress)", new
                    {
                        record.Passport.LocDocHolderCustomProfile.Formno,
                        record.Passport.LocDocHolderCustomProfile.Othername1,
                        record.Passport.LocDocHolderCustomProfile.Othername2,
                        record.Passport.LocDocHolderCustomProfile.Othername3,
                        record.Passport.LocDocHolderCustomProfile.Othername4,
                        record.Passport.LocDocHolderCustomProfile.Dobflag,
                        record.Passport.LocDocHolderCustomProfile.Originstate,
                        record.Passport.LocDocHolderCustomProfile.Addstreet,
                        record.Passport.LocDocHolderCustomProfile.Addtown,
                        record.Passport.LocDocHolderCustomProfile.Addstate,
                        record.Passport.LocDocHolderCustomProfile.Profession,
                        record.Passport.LocDocHolderCustomProfile.Occupation,
                        record.Passport.LocDocHolderCustomProfile.Height,
                        record.Passport.LocDocHolderCustomProfile.Eyecolor,
                        record.Passport.LocDocHolderCustomProfile.Haircolor,
                        record.Passport.LocDocHolderCustomProfile.Maritalstatus,
                        record.Passport.LocDocHolderCustomProfile.Birthmark,
                        record.Passport.LocDocHolderCustomProfile.Maidenname,
                        record.Passport.LocDocHolderCustomProfile.Nokname,
                        record.Passport.LocDocHolderCustomProfile.Nokaddress,

                        record.Passport.LocDocHolderCustomProfile.Child1Sname,
                        record.Passport.LocDocHolderCustomProfile.Child1Fname,
                        record.Passport.LocDocHolderCustomProfile.Child1Dob,
                        record.Passport.LocDocHolderCustomProfile.Child1Pobt,
                        record.Passport.LocDocHolderCustomProfile.Child1Pobs,
                        record.Passport.LocDocHolderCustomProfile.Child1Sex,

                        record.Passport.LocDocHolderCustomProfile.Child2Sname,
                        record.Passport.LocDocHolderCustomProfile.Child2Fname,
                        record.Passport.LocDocHolderCustomProfile.Child2Dob,
                        record.Passport.LocDocHolderCustomProfile.Child2Pobt,
                        record.Passport.LocDocHolderCustomProfile.Child2Pobs,
                        record.Passport.LocDocHolderCustomProfile.Child2Sex,

                        record.Passport.LocDocHolderCustomProfile.Child3Sname,
                        record.Passport.LocDocHolderCustomProfile.Child3Fname,
                        record.Passport.LocDocHolderCustomProfile.Child3Dob,
                        record.Passport.LocDocHolderCustomProfile.Child3Pobt,
                        record.Passport.LocDocHolderCustomProfile.Child3Pobs,
                        record.Passport.LocDocHolderCustomProfile.Child3Sex,

                        record.Passport.LocDocHolderCustomProfile.Child4Sname,
                        record.Passport.LocDocHolderCustomProfile.Child4Fname,
                        record.Passport.LocDocHolderCustomProfile.Child4Dob,
                        record.Passport.LocDocHolderCustomProfile.Child4Pobt,
                        record.Passport.LocDocHolderCustomProfile.Child4Pobs,
                        record.Passport.LocDocHolderCustomProfile.Child4Sex,

                        record.Passport.LocDocHolderCustomProfile.Title,
                        record.Passport.LocDocHolderCustomProfile.Eyecolorother,
                        record.Passport.LocDocHolderCustomProfile.Haircolorother,
                        record.Passport.LocDocHolderCustomProfile.Addpermanent,
                        record.Passport.LocDocHolderCustomProfile.Email,
                        record.Passport.LocDocHolderCustomProfile.Contactphone,
                        record.Passport.LocDocHolderCustomProfile.Mobilephone,
                        record.Passport.LocDocHolderCustomProfile.Gtoname,
                        record.Passport.LocDocHolderCustomProfile.Gtoaddress
                    });

                // DocHolderBioProfile
                if (!string.IsNullOrEmpty(bioprofileFormNo))
                {
                    var oldbioprofile = dbConn
                        .Query<LocDocHolderBioProfile>("select * from loc_docholderbioprofile where formno = @formno",
                            new {rec.FormNo}).SingleOrDefault();

                    // replace all except signature
                    var sign = record.Passport.LocDocHolderBioProfile.Signimage;

                    if (oldbioprofile != null)
                    {
                        record.Passport.LocDocHolderBioProfile = oldbioprofile;

                        if (sign != null)
                            record.Passport.LocDocHolderBioProfile.Signimage = sign;
                    }
                }

                record.Passport.LocDocHolderBioProfile.Formno = newformno;
                dbConn.Execute(
                        "insert into loc_docholderbioprofile (formno, faceimage, faceimagej2k, faceentrytime, finger1image, finger1template1, finger1template2, finger1code, finger1reason, finger2image, " +
                        "finger2template1, finger2template2, finger2code, finger2reason, finger3image, finger3template1, finger3template2, finger3code, finger3reason, finger4image, finger4template1, " +
                        "finger4template2, finger4code, finger4reason, fingerentrytime, signimage, signentrytime) " +
                        "values (@formno, @faceimage, @faceimagej2k, @faceentrytime, @finger1image, @finger1template1, @finger1template2, @finger1code, @finger1reason, " +
                        "@finger2image, @finger2template1, @finger2template2, @finger2code, @finger2reason, @finger3image, @finger3template1, @finger3template2, @finger3code, @finger3reason, @finger4image, " +
                        "@finger4template1, @finger4template2, @finger4code, @finger4reason, @fingerentrytime, @signimage, @signentrytime)",
                        new
                        {
                            record.Passport.LocDocHolderBioProfile.Formno,
                            record.Passport.LocDocHolderBioProfile.Faceimage,
                            record.Passport.LocDocHolderBioProfile.Faceimagej2K,
                            record.Passport.LocDocHolderBioProfile.Faceentrytime,
                            record.Passport.LocDocHolderBioProfile.Finger1Image,
                            record.Passport.LocDocHolderBioProfile.Finger1Template1,
                            record.Passport.LocDocHolderBioProfile.Finger1Template2,
                            record.Passport.LocDocHolderBioProfile.Finger1Code,
                            record.Passport.LocDocHolderBioProfile.Finger1Reason,
                            record.Passport.LocDocHolderBioProfile.Finger2Image,
                            record.Passport.LocDocHolderBioProfile.Finger2Template1,
                            record.Passport.LocDocHolderBioProfile.Finger2Template2,
                            record.Passport.LocDocHolderBioProfile.Finger2Code,
                            record.Passport.LocDocHolderBioProfile.Finger2Reason,
                            record.Passport.LocDocHolderBioProfile.Finger3Image,
                            record.Passport.LocDocHolderBioProfile.Finger3Template1,
                            record.Passport.LocDocHolderBioProfile.Finger3Template2,
                            record.Passport.LocDocHolderBioProfile.Finger3Code,
                            record.Passport.LocDocHolderBioProfile.Finger3Reason,
                            record.Passport.LocDocHolderBioProfile.Finger4Image,
                            record.Passport.LocDocHolderBioProfile.Finger4Template1,
                            record.Passport.LocDocHolderBioProfile.Finger4Template2,
                            record.Passport.LocDocHolderBioProfile.Finger4Code,
                            record.Passport.LocDocHolderBioProfile.Finger4Reason,
                            record.Passport.LocDocHolderBioProfile.Fingerentrytime,
                            record.Passport.LocDocHolderBioProfile.Signimage,
                            record.Passport.LocDocHolderBioProfile.Signentrytime
                        });

                // for integrated branches
                try
                {
                    dbConn.Execute(
                        "update loc_docholderbioprofile set finger5image=@finger5image, finger5template1=@finger5template1, finger5template2=@finger5template2, finger5code=@finger5code, finger5reason=@finger5reason, " +
                        "finger6image=@finger6image, finger6template1=@finger6template1, finger6template2=@finger6template2, finger6code=@finger6code, finger6reason=@finger6reason," +
                        "finger7image=@finger7image, finger7template1=@finger7template1, finger7template2=@finger7template2, finger7code=@finger7code, finger7reason=@finger7reason," +
                        "finger8image=@finger8image, finger8template1=@finger8template1, finger8template2=@finger8template2, finger8code=@finger8code, finger8reason=@finger8reason," +
                        "finger9image=@finger9image, finger9template1=@finger9template1, finger9template2=@finger9template2, finger9code=@finger9code, finger9reason=@finger9reason," +
                        "finger10image=@finger10image, finger10template1=@finger10template1, finger10template2=@finger10template2, finger10code=@finger10code, finger10reason=@finger10reason " +
                        "where formno = @formno",
                        new
                        {
                            record.Passport.LocDocHolderBioProfile.Formno,
                            record.Passport.LocDocHolderBioProfile.Finger5Image,
                            record.Passport.LocDocHolderBioProfile.Finger5Template1,
                            record.Passport.LocDocHolderBioProfile.Finger5Template2,
                            record.Passport.LocDocHolderBioProfile.Finger5Code,
                            record.Passport.LocDocHolderBioProfile.Finger5Reason,
                            record.Passport.LocDocHolderBioProfile.Finger6Image,
                            record.Passport.LocDocHolderBioProfile.Finger6Template1,
                            record.Passport.LocDocHolderBioProfile.Finger6Template2,
                            record.Passport.LocDocHolderBioProfile.Finger6Code,
                            record.Passport.LocDocHolderBioProfile.Finger6Reason,
                            record.Passport.LocDocHolderBioProfile.Finger7Image,
                            record.Passport.LocDocHolderBioProfile.Finger7Code,
                            record.Passport.LocDocHolderBioProfile.Finger7Template1,
                            record.Passport.LocDocHolderBioProfile.Finger7Template2,
                            record.Passport.LocDocHolderBioProfile.Finger7Reason,
                            record.Passport.LocDocHolderBioProfile.Finger8Image,
                            record.Passport.LocDocHolderBioProfile.Finger8Code,
                            record.Passport.LocDocHolderBioProfile.Finger8Template1,
                            record.Passport.LocDocHolderBioProfile.Finger8Template2,
                            record.Passport.LocDocHolderBioProfile.Finger8Reason,
                            record.Passport.LocDocHolderBioProfile.Finger9Image,
                            record.Passport.LocDocHolderBioProfile.Finger9Code,
                            record.Passport.LocDocHolderBioProfile.Finger9Template1,
                            record.Passport.LocDocHolderBioProfile.Finger9Template2,
                            record.Passport.LocDocHolderBioProfile.Finger9Reason,
                            record.Passport.LocDocHolderBioProfile.Finger10Image,
                            record.Passport.LocDocHolderBioProfile.Finger10Code,
                            record.Passport.LocDocHolderBioProfile.Finger10Template1,
                            record.Passport.LocDocHolderBioProfile.Finger10Template2,
                            record.Passport.LocDocHolderBioProfile.Finger10Reason
                        });
                }
                catch (Exception ex)
                {
                    // Do nothing
                }
                
                // PaymentHistory
                record.Passport.LocPaymentHistory.Formno = newformno;
                dbConn.Execute(
                    @"insert into loc_paymenthistory (formno, bankdraftno, pymttime, pymtrecvby, pymtamt, bankname, receiptno, refno, appid) 
                            values (@formno, @bankdraftno, @pymttime, @pymtrecvby, @pymtamt, @bankname, @receiptno, @refno, @appid)",
                    new
                    {
                        record.Passport.LocPaymentHistory.Formno,
                        record.Passport.LocPaymentHistory.Bankdraftno,
                        record.Passport.LocPaymentHistory.Pymttime,
                        record.Passport.LocPaymentHistory.Pymtrecvby,
                        record.Passport.LocPaymentHistory.Pymtamt,
                        record.Passport.LocPaymentHistory.Bankname,
                        record.Passport.LocPaymentHistory.Receiptno,
                        refno = rec.RefId,
                        appid = rec.AppId
                    });
            }
        }

        public LocPersoProfile GetPersoProfile(string docNo, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return null;

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where docno = @docno", new { docNo }).SingleOrDefault();
                return persoProfile;}
        }

        public LocPersoProfile GetPersoProfileWithFormNo(string formno, string branchcode, int userId)
        {
            var branch = GetConnectionString(branchcode, userId);

            if (branch == null)
                return null;

            using (var dbConn = new SqlConnection(branch.conn))
            {
                dbConn.Open();

                var persoProfile = dbConn.Query<LocPersoProfile>("select * from loc_persoprofile where formno = @formno", new { formno }).FirstOrDefault();
                return persoProfile;
            }
        }

        private void AddToAudit(Audit audit)
        {
            using (var dbConn = new SqlConnection(_auth))
            {
                dbConn.Open();
                dbConn.Execute("insert into Audits (eventid, timestamp, userid, description, formno, docno) values (@eventid, @timestamp, @userid, @description, @formno, @docno)",
                    new
                    {
                        audit.EventId,
                        audit.TimeStamp,
                        audit.UserId,
                        audit.Description,
                        audit.FormNo,
                        audit.DocNo
                    });
            }
        }

        private void AddBiometrics(DetailedRecord record, LocDocHolderBioProfile bioProfile)
        {
            record.Fingerprints = new List<Fingerprint>();

            record.FaceCurrent = bioProfile?.Faceimage;
            record.SignatureCurrent = bioProfile?.Signimage;

            // Fingerprints
            if (bioProfile?.Finger1Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger1Code,
                    Finger = bioProfile.Finger1Image
                });
            }

            if (bioProfile?.Finger2Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger2Code,
                    Finger = bioProfile.Finger2Image
                });
            }

            if (bioProfile?.Finger3Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger3Code,
                    Finger = bioProfile.Finger3Image
                });
            }

            if (bioProfile?.Finger4Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger4Code,
                    Finger = bioProfile.Finger4Image
                });
            }

            if (bioProfile?.Finger5Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger5Code,
                    Finger = bioProfile.Finger5Image
                });
            }

            if (bioProfile?.Finger6Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger6Code,
                    Finger = bioProfile.Finger6Image
                });
            }

            if (bioProfile?.Finger7Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger7Code,
                    Finger = bioProfile.Finger7Image
                });
            }

            if (bioProfile?.Finger8Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger8Code,
                    Finger = bioProfile.Finger8Image
                });
            }

            if (bioProfile?.Finger9Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger9Code,
                    Finger = bioProfile.Finger9Image
                });
            }

            if (bioProfile?.Finger10Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger10Code,
                    Finger = bioProfile.Finger10Image
                });
            }
        }

        private Branch GetConnectionString(string branchcode, int oprid)
        {
            if (bool.Parse(ConfigurationManager.AppSettings["useLaptopAsBranch"]))
            {
                return new Branch
                {
                    BranchCode = "001",
                    BranchName = "Laptop",
                    conn = ConfigurationManager.ConnectionStrings["DatabaseLocal"].ConnectionString
                };
            }

            var branches = Helper.GetBranches();
            var branch = branches.FirstOrDefault(x => x.BranchCode == branchcode);

            if (branch == null)
                return null;

            // check if user is restricted to certain branches
            var userbranches = new AuthService().GetMemberBranch(oprid);

            if (userbranches.SelectedBranches.Count != 0 &&
                userbranches.SelectedBranches.All(x => x.BranchCode != branch.BranchCode))
                return null;

            // simple decryption of username/pw from config file. Basically i "salt" it with random characters which I remove in the code based on the "salt" value in config
            var encrypted = ConfigurationManager.AppSettings["branchConnectionString"];
            var decrypted = Helper.DecryptConfig(encrypted);

            string connString = $"Server={branch?.Ip}; {decrypted}";

            if (bool.Parse(ConfigurationManager.AppSettings["useLaptopAsBranch"]))
                connString = ConfigurationManager.ConnectionStrings["DatabaseLocal"].ConnectionString;

            branch.conn = connString;
            return branch;
        }

    }
}
