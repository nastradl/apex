﻿using System.Threading.Tasks;
using Apex.Repositories;

namespace Apex.Services
{
    public class EmailSender : IEmailSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Task.CompletedTask;
        }
    }
}
