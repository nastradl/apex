﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Apex.Startup))]
namespace Apex
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
