﻿using System;

namespace Apex.ViewModels
{
    public class Booklet
    {
        public int Id { get; set; }

        public string DocType { get; set; }

        public string DocNo { get; set; }

        public string BranchCode { get; set; }

        public string BoxSn { get; set; }

        public string ChipSn { get; set; }

        public DateTime EntryTime { get; set; }

        public string DocPage { get; set; }

        public int Sent { get; set; }

        public string StageCode { get; set; }

        public DateTime DateCreated { get; set; }
    }
}