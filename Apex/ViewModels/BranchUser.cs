﻿namespace Apex.ViewModels
{
    public class JsonResponse
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}