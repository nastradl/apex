﻿using System;

namespace Apex.ViewModels
{
    public class CheckDate
    {
        public string Formno { get; set; }

        public DateTime? PreviousDate { get; set; }

        public DateTime? NewDate { get; set; }
    }
}