﻿using Apex.Models.Central;
using System.Collections.Generic;

namespace Apex.ViewModels
{
    public class EditSubclassWrap
    {
        public string SubclassId { get; set; }

        public List<ClaimsSubclass> Subclasses { get; set; }
    }
}
