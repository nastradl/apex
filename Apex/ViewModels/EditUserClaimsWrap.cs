﻿using System.Collections.Generic;

namespace Apex.ViewModels
{
    public class EditUserClaimsWrap
    {
        public string UserId { get; set; }

        public List<EditUserClaims> Claims { get; set; }
    }
}
