﻿using System.Collections.Generic;

namespace Apex.ViewModels
{
    public class MemberBranch
    {
        public MemberBranch()
        {
            SelectedBranches = new List<Branch>();
            UnSelectedBranches = new List<Branch>();
        }

        public int Id { get; set; }

        public int MemberId { get; set; }

        public List<Branch> SelectedBranches { get; set; }

        public List<Branch> UnSelectedBranches { get; set; }
    }
}