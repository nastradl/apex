﻿using System.Collections.Generic;

namespace Apex.ViewModels
{
    public class MemberBranchWrap
    {
        public string Id { get; set; }

        public List<MemberBranch> MemberBranches { get; set; }
    }
}