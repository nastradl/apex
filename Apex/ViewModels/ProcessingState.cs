﻿namespace Apex.ViewModels
{
    public class ProcessingState
    {
        public int StateId { get; set; }

        public string StateName { get; set; }
    }
}