﻿namespace Apex.ViewModels
{
    public class SearchParam
    {
        public int PerPage { get; set; }

        public string UserId { get; set; }

        public string Actions { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string FormNo { get; set; }

        public string DocNo { get; set; }

        public int Page { get; set; }

        public string OrderBy { get; set; }

        public int IsPrint { get; set; } = 0;

        public int PrintPage { get; set; } = 0;

        public string Reverse { get; set; }
    }
}